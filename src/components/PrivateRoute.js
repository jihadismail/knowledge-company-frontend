import React from 'react';
import { Route, Redirect } from 'react-router-dom';

export const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      localStorage.getItem('admin') ? (
        <Component {...props} {...rest} />
      ) : (
        <Redirect
          to={{ pathname: '/login', state: { from: props.location } }}
        />
      )
    }
  />
);

export const GuestRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props => {
      const user = JSON.parse(localStorage.getItem('admin'));
      return user ? (
        <Redirect
          to={{
            pathname: '/' + user.organization.permalink,
            state: { from: props.location }
          }}
        />
      ) : (
        <Component {...props} />
      );
    }}
  />
);
