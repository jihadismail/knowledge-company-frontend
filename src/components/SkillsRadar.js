import React, { Fragment } from 'react';
import { Radar } from 'react-chartjs-2';

class SkillsRadar extends React.Component {
  render() {
    const { data } = this.props;

    let chartOptions = {
      responsive: true,
      maintainAspectRatio: false,
      scale: {
        ticks: {
          stepSize: 15
        },
        pointLabels: {
          fontSize: 14
        }
      },
      legend: {
        display: false,
        position: 'left'
      }
    };

    return (
      <Fragment>
        <Radar data={data} width="350" height="300" options={chartOptions} />
      </Fragment>
    );
  }
}

export { SkillsRadar };
