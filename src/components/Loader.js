import React from 'react';
//import loadingimg from './loader.svg';
//import loadingimginv from './loader-inv.svg';

class Loading extends React.Component {
  render() {
    const { relative } = this.props;
    return (
      <span className={`loading-component ${relative ? 'relative' : ''}`}>
        <i class="fas fa-spinner fa-spin" />
      </span>
    );
  }
}

export const Loader = ({ isLoading, component: Component, ...rest }) =>
  isLoading ? <Loading /> : <Component {...rest} />;

export { Loading };
