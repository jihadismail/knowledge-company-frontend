import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { adminActions } from 'actions';
// import { woopra_track_event } from 'helpers';

// images
import ko_logo from './ko-logo.svg';
import icon_android_white from './images/icon-android-white.svg';
import icon_ios_white from './images/icon-ios-white.svg';

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
    this.handleOutsideClick = this.handleOutsideClick.bind(this);
    this.state = {
      condition: false,
      menuOpened: false
    };
  }
  handleClick() {
    if (!this.state.menuOpened) {
      document.addEventListener('click', this.handleOutsideClick, false);
    } else {
      document.removeEventListener('click', this.handleOutsideClick, false);
    }

    this.setState(prevState => ({
      menuOpened: !prevState.menuOpened
    }));
  }

  handleOutsideClick(e) {
    // ignore clicks on the component itself
    if (this.node.contains(e.target)) {
      return;
    }
    this.handleClick();
  }

  handleLogout = e => {
    e.preventDefault();
    this.props.dispatch(adminActions.logout());
    // woopra_track_event('click logout', { view_name: 'website' });
  };

  trackLogin = () => {
    // woopra_track_event('click login', { view_name: 'website' });
  };

  render() {
    const { user, company, employee } = this.props;
    let button = null;
    if (user) {
      button = [
        <li key="blank-li-holder" className="blank-li" />,
        <li key="li-settings">
          {/* <Link className="nav-link" to="/settings/account">
            Invite
          </Link> */}
        </li>,
        <li key="li-user" className="user-li">
          <a className="app-icon">
            <img src={icon_android_white} />
          </a>
          <a className="app-icon">
            <img src={icon_ios_white} />
          </a>
          <span>
            <b>{user.email}</b>
          </span>
        </li>,
        <li key="li-logout">
          <a
            className="btn btn--dark user-auth-action-btn signout-button shrink-signout-button"
            onClick={this.handleLogout}
          >
            <i className="fas fa-sign-out-alt" /> Logout
          </a>
        </li>
      ];
    } else {
      button = [<li key="blank-li" className="blank-li" />];
    }
    let companyid = JSON.parse(localStorage.getItem('companyId'));
    if (!companyid && company) {
      companyid = company.permalink;
    }
    if (!companyid && employee) {
      companyid = employee.organization.permalink;
    }

    return (
      <header>
        <nav className="navbar">
          <Link to={`/${companyid}`} className="navbar-brand">
            <img src={ko_logo} alt="Knowledge Officer Logo" />
          </Link>
          <button
            className="mobile-navbar-btn"
            onClick={this.handleClick}
            ref={node => {
              this.node = node;
            }}
          >
            <i className="fas fa-bars" />
          </button>
          <ul
            className={
              this.state.menuOpened
                ? 'nav navbar-nav opened'
                : 'nav navbar-nav navbar-expand-md'
            }
          >
            {button}
          </ul>
        </nav>
      </header>
    );
  }
}

function mapStateToProps(state) {
  const { authentication, users, companies, employees } = state;
  const user = users.user || authentication.user;
  const company = companies.company;
  const employee = employees.employee;
  return {
    user,
    company,
    employee
  };
}

const HeaderComponent = connect(mapStateToProps)(Header);
export { HeaderComponent as Header };
