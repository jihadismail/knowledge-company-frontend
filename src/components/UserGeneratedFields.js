import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { companyActions } from 'actions';

import './UserGeneratedFields.css';

class UserGeneratedFields extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      content_add: '',
      members: [],
      lastId: -1
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.inviteMembersError) {
      this.setState({ error: true });
    }
    if (nextProps.inviteMembersSuccess) {
      this.setState({ success: true });
    }
    setTimeout(() => this.props.onDone(), 2000);
  }

  handleFocus = event => {
    this.setState({ content_add: '' });
  };

  handleChange = event => {
    const usr_input = event.target.value;
    this.setState({ content_add: usr_input });
  };

  handleKeypress = event => {
    if (event.key === 'Enter') {
      this.addNewInputField();
    }
  };

  handleBlur = event => {
    document.querySelector('#add').focus();
    this.addNewInputField();
  };

  handleClickAddInput = () => {
    this.addNewInputField();
  };

  addNewInputField = () => {
    const currentcontent = this.state.content_add.trim();
    if (!currentcontent) {
      return;
    }

    document.querySelector('#add').focus();

    let newItem = {
      email: currentcontent,
      id: this.state.lastId + 1
    };
    this.setState(prevState => {
      return {
        members: [...prevState.members, newItem],
        content_add: '',
        lastId: newItem.id
      };
    });
  };

  handleRemove = (e, listitemId) => {
    const idToRemove = Number(listitemId);
    const newArray = this.state.members.filter(listitem => {
      return listitem.id !== idToRemove;
    });
    this.setState({ members: newArray });
  };

  makeAddedList = () => {
    const elements = this.state.members.map((listitem, index) => (
      <li key={listitem.id} data-item={listitem.id}>
        {listitem.email}
        <span
          className="remove"
          onClick={e => this.handleRemove(e, listitem.id)}
        />
      </li>
    ));
    return elements;
  };

  handleSendInvites = e => {
    e.preventDefault();
    this.props.dispatch(
      companyActions.inviteMembers({ members: this.state.members })
    );
  };

  render() {
    const { members } = this.state;
    return (
      <Fragment>
        <h1>Enter emails</h1>
        <p>
          We’ll send your employees an email with instructions on signing up.
        </p>
        {this.state.success || this.state.error ? (
          <Fragment>
            {this.state.success && (
              <h4 style={{ color: 'light green' }}>
                Invitations sent successfully
              </h4>
            )}
            {this.state.error && (
              <h4 style={{ color: 'red' }}>Sorry, something went wrong</h4>
            )}
          </Fragment>
        ) : (
          <Fragment>
            <div id="UserGeneratedFields" className="emails-input-list">
              <ul>{this.makeAddedList()}</ul>
              <input
                id="add"
                type="email"
                name="initvalue"
                onFocus={this.handleFocus}
                onChange={this.handleChange}
                onKeyPress={this.handleKeypress}
                onBlur={this.handleBlur}
                value={this.state.content_add}
                placeholder={'✉️ Add email'}
                autoFocus={true}
              />
            </div>
            <div className="action-btn-wrapper">
              <button
                className="ko-btn-primary"
                onClick={this.handleSendInvites}
                disabled={members.length === 0 ? 'disabled' : ''}
              >
                Send invites
              </button>
            </div>
          </Fragment>
        )}
      </Fragment>
    );
  }
}

function mapStateToProps(state) {
  const { companies } = state;
  const {
    inviteMembersLoading,
    inviteMembersSuccess,
    inviteMembersError
  } = companies;
  return {
    inviteMembersLoading,
    inviteMembersSuccess,
    inviteMembersError
  };
}

const connectedUserGeneratedFields = connect(mapStateToProps)(
  UserGeneratedFields
);
export { connectedUserGeneratedFields as UserGeneratedFields };
