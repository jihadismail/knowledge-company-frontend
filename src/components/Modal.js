import React, { Fragment } from 'react';
import { woopra_track_view, woopra_track_event } from 'helpers';

import './Modal.css';

class Modal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: true,
      fadeOutClass: ''
    };
    this.handleButtonClick = this.handleButtonClick.bind(this);
  }

  componentDidMount() {
    if (this.props.woopraAttributes) {
      woopra_track_view(this.props.woopraViewName, this.props.woopraAttributes);
    } else {
      woopra_track_view(this.props.woopraViewName);
    }
  }

  handleButtonClick(e, buttonName) {
    e.preventDefault();
    this.setState({
      fadeOutClass: 'modal-fade-out'
    });
    setTimeout(() => {
      this.setState({
        showModal: false,
        fadeOutClass: ''
      });
      this.props.onDismiss();
    }, 500);

    if (buttonName === 'Dismissed') {
      woopra_track_event('click dismiss', {
        view_name: this.props.woopraViewName
      });
    }
  }

  render() {
    const { showModal, fadeOutClass } = this.state;
    const {
      showCloseButton,
      children,
      showSecondayDismissButton,
      dismissButtonText,
      modalClass
    } = this.props;

    return (
      <Fragment>
        {showModal && (
          <div
            id=""
            className={`ko-modal ${
              modalClass ? modalClass : ''
            } ${fadeOutClass}`}
          >
            <div className="modal-wrapper">
              {showCloseButton && (
                <button
                  className="close-modal"
                  onClick={e => this.handleButtonClick(e, 'Dismissed')}
                >
                  x
                </button>
              )}
              {children}
              {showSecondayDismissButton && (
                <button
                  className="seconday-link"
                  onClick={e => this.handleButtonClick(e, 'Dismissed')}
                >
                  {dismissButtonText}
                </button>
              )}
            </div>
          </div>
        )}
      </Fragment>
    );
  }
}

export { Modal };
