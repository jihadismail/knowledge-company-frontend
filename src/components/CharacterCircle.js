import React from 'react';

import './CharacterCircle.css';

class CharacterCircle extends React.Component {
  render() {
    const {
      compactSize, // true or false
      avatarSize, // 'c-90' or 'c-60'
      backgroundColor, // 'light' or 'dark'
      speechBubbleMessage,
      characterClass
    } = this.props;

    return (
      <div
        className={`circle-character ${avatarSize} ${
          compactSize ? 'compact-size' : ''
        }`}
      >
        <div className={`speech-bubble ${backgroundColor}`}>
          <p>{speechBubbleMessage}</p>
        </div>
        <div className={`${characterClass}`}>
          <span />
        </div>
      </div>
    );
  }
}

export { CharacterCircle };
