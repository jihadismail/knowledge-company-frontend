import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { UserGeneratedFields } from './UserGeneratedFields';
import { Modal } from 'components';
import CSVReader from 'react-csv-reader';
import { companyActions } from 'actions';

import illu_bg from './images/illu-bg.png';
import tick_shape from './images/shape.svg';
import cross_shape from './images/cross-shape.svg';
const tick_shape_style = {
  marginTop: '0px'
};
const cross_shape_style = {
  transform: 'translate(-10px, 0px)'
};

class InviteEmployeeModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showEmailsInvitationForm: false,
      csvImportSuccess: false,
      csvImportFail: false,
      loadingCSV: false,
      emailSentToCSVSuccess: false,
      members: []
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.inviteMembersError) {
      this.setState({ error: true });
    }
    if (nextProps.inviteMembersSuccess) {
      this.setState({ emailSentToCSVSuccess: true });
    }
    setTimeout(() => this.handleDismissModal(), 5000);
  }

  handleDismissModal = () => {
    this.props.handleHideInvitationModal();
  };

  handleClick = emailType => {
    if (emailType === 'emails') {
      this.setState({
        showEmailsInvitationForm: true
      });
    }
  };

  handleFileSelect = e => {
    e.preventDefault();
    document.querySelector('#csvfile').click();
    this.setState({ loadingCSV: true });
  };

  importCSV = data => {
    if (
      data[0][0].toUpperCase() === 'NAME' &&
      data[0][1].toUpperCase() === 'EMAIL'
    ) {
      //remove the header
      data.shift();
      data = data.filter(item => {
        return item[0] && item[1];
      });
      let members = data.map((item, index) => {
        return { name: item[0], email: item[1], id: index };
      });
      document.querySelector('#csvfile').value = '';
      this.setState({
        csvImportSuccess: true,
        loadingCSV: false,
        members: members
      });
    } else {
      this.handleImportError();
    }
  };

  handleImportError = e => {
    document.querySelector('#csvfile').value = '';
    this.setState({ csvImportFail: true, loadingCSV: false, members: [] });
    setTimeout(() => this.setState({ csvImportFail: false }), 4000);
  };

  handleSendInvites = () => {
    this.props.dispatch(
      companyActions.inviteMembers({ members: this.state.members })
    );
  };

  render() {
    const {
      showEmailsInvitationForm,
      csvImportSuccess,
      csvImportFail,
      loadingCSV,
      emailSentToCSVSuccess,
      members
    } = this.state;
    return (
      <Modal
        key={'introModal'}
        showCloseButton={true}
        showSecondayDismissButton={false}
        dismissButtonText={'Not right now'}
        modalClass={'invite-employees'}
        onDismiss={this.handleDismissModal}
      >
        <div className="scrollable">
          <img src={illu_bg} className="illu" alt="" />
          {!showEmailsInvitationForm &&
            !csvImportSuccess && (
              <Fragment>
                <h1>Invite your employees</h1>
                <p>
                  Invite employees by adding their emails or importing their
                  emails from a csv file.
                </p>
                <div className="action-btn-wrapper">
                  <button
                    className="ko-btn-primary"
                    onClick={e => this.handleClick('emails')}
                  >
                    Enter emails
                  </button>
                  <CSVReader
                    onFileLoaded={this.importCSV}
                    onError={this.handleImportError}
                    inputId="csvfile"
                    inputStyle={{ display: 'none' }}
                  />
                  {!csvImportFail && (
                    <button
                      className="ko-btn-primary"
                      onClick={this.handleFileSelect}
                    >
                      {!loadingCSV && 'Import a csv'}
                      {loadingCSV && <i class="fas fa-spinner fa-spin" />}
                    </button>
                  )}
                  {csvImportFail && (
                    <button
                      className="ko-btn-primary error-btn"
                      onClick={this.handleFileSelect}
                    >
                      <img src={cross_shape} style={cross_shape_style} alt="" />{' '}
                      Import failed
                    </button>
                  )}
                </div>
              </Fragment>
            )}
          {showEmailsInvitationForm && (
            <UserGeneratedFields onDone={this.handleDismissModal} />
          )}
          {csvImportSuccess && (
            <Fragment>
              <h1>We’ve imported {members.length} emails</h1>
              <p>
                We’ll send your employees an email with instructions on signing
                up.
              </p>
              <div className="action-btn-wrapper">
                {!emailSentToCSVSuccess &&
                  !this.state.error && (
                    <button
                      className="ko-btn-primary"
                      onClick={this.handleSendInvites}
                    >
                      Send invites
                    </button>
                  )}
                {emailSentToCSVSuccess && (
                  <button className="ko-btn-primary success-btn">
                    <img src={tick_shape} style={tick_shape_style} alt="" />
                  </button>
                )}
                {this.state.error && (
                  <h4 style={{ color: 'red' }}>Sorry, something went wrong</h4>
                )}
              </div>
            </Fragment>
          )}
        </div>
      </Modal>
    );
  }
}

function mapStateToProps(state) {
  const { companies } = state;
  const {
    inviteMembersLoading,
    inviteMembersSuccess,
    inviteMembersError
  } = companies;
  return {
    inviteMembersLoading,
    inviteMembersSuccess,
    inviteMembersError
  };
}

const connectedInviteEmployeeModal = connect(mapStateToProps)(
  InviteEmployeeModal
);
export { connectedInviteEmployeeModal as InviteEmployeeModal };
