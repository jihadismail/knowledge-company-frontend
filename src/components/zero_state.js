import React from 'react';

import './zero_state.css';

class ZeroState extends React.Component {
  render() {
    const { text, actionText, action } = this.props;

    return (
      <article className="zero-state">
        <p>{text}</p>
        {action && (
          <button onClick={action} className="ko-btn-primary">
            {actionText}
          </button>
        )}
      </article>
    );
  }
}

export { ZeroState };
