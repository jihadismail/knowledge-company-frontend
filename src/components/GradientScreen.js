import React from 'react';
import { woopra_track_view, woopra_track_event } from 'helpers';

import './GradientScreen.css';

class GradientScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showGradientScreen: true,
      fadeOutClass: ''
    };
    this.handleButtonClick = this.handleButtonClick.bind(this);
  }

  componentDidMount() {
    if (this.props.woopraAttributes) {
      woopra_track_view(this.props.woopraViewName, this.props.woopraAttributes);
    } else {
      woopra_track_view(this.props.woopraViewName);
    }

    if (this.props.autoHide !== undefined && this.props.autoHide === true) {
      setTimeout(() => {
        this.setState({
          fadeOutClass: 'modal-fade-out'
        });
        setTimeout(() => {
          this.setState({
            fadeOutClass: ''
          });
          this.props.onDismiss && this.props.onDismiss();
        }, 800);
      }, this.props.hideWaitTime);
    }
  }

  handleButtonClick(e, buttonName) {
    e.preventDefault();
    this.setState({
      fadeOutClass: 'modal-fade-out'
    });
    setTimeout(() => {
      this.setState({
        fadeOutClass: ''
      });
      this.props.onDismiss && this.props.onDismiss();
    }, 800);

    if (buttonName === 'Dismissed') {
      woopra_track_event('click dismiss', {
        view_name: this.props.woopraViewName
      });
    } else if (buttonName === 'Continue') {
      woopra_track_event('click continue', {
        view_name: this.props.woopraViewName
      });
    }
  }

  render() {
    const { fadeOutClass } = this.state;
    const {
      showCloseButton,
      children,
      showSecondayDismissButton,
      dismissButtonText,
      dismissButtonColor,
      screenClass,
      hideGradient,
      hidePattern
    } = this.props;

    return (
      <div
        className={`gradient-screen ${
          screenClass ? screenClass : ''
        } ${fadeOutClass}
        ${hideGradient ? 'hide-gradient' : ''}
        ${hidePattern ? 'hide-pattern' : ''}
        `}
      >
        <div className="content-wrapper">
          {showCloseButton && (
            <button
              className="close-modal"
              onClick={e => this.handleButtonClick(e, 'Dismissed')}
            >
              {' '}
              x{' '}
            </button>
          )}
          {children}
          {showSecondayDismissButton && (
            <button
              className={`seconday-link ${dismissButtonColor &&
                dismissButtonColor}`}
              onClick={e => this.handleButtonClick(e, 'Continue')}
            >
              {dismissButtonText}
            </button>
          )}
        </div>
      </div>
    );
  }
}

export { GradientScreen };
