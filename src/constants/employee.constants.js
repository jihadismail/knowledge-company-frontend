export const employeeConstants = {
  GET_REQUEST: 'EMPLOYEE_GET_REQUEST',
  GET_SUCCESS: 'EMPLOYEE_GET_SUCCESS',
  GET_FAILURE: 'EMPLOYEE_GET_FAILURE',
  GET_SKILLS_REQUEST: 'EMPLOYEE_GET_SKILLS_REQUEST',
  GET_SKILLS_SUCCESS: 'EMPLOYEE_GET_SKILLS_SUCCESS',
  GET_SKILLS_FAILURE: 'EMPLOYEE_GET_SKILLS_FAILURE',
  GET_SKILLS_GAP_REQUEST: 'EMPLOYEE_GET_SKILLS_GAP_REQUEST',
  GET_SKILLS_GAP_SUCCESS: 'EMPLOYEE_GET_SKILLS_GAP_SUCCESS',
  GET_SKILLS_GAP_FAILURE: 'EMPLOYEE_GET_SKILLS_GAP_FAILURE',
  GET_SKILLS_RADAR_REQUEST: 'EMPLOYEE_GET_SKILLS_RADAR_REQUEST',
  GET_SKILLS_RADAR_SUCCESS: 'EMPLOYEE_GET_SKILLS_RADAR_SUCCESS',
  GET_SKILLS_RADAR_FAILURE: 'EMPLOYEE_GET_SKILLS_RADAR_FAILURE'
};
