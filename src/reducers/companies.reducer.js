import { companyConstants } from '../constants';

const INITIAL_STATE = {
  isLoading: true,
  company: null,
  companyTopSkills: null,
  isLoadingTopSkills: true,
  companySkillsGap: null,
  isLoadingSkillsGap: true,
  employees: null,
  isLoadingEmployees: true,
  error: null,
  isLoadingPersonas: true,
  personas: null,
  inviteMembersLoading: false,
  inviteMembersSuccess: null,
  inviteMembersError: null,
  getInTouchLoading: false,
  getInTouchSuccess: null,
  getInTouchError: null,
  peopleAnalytics: null
};

export function companies(state = INITIAL_STATE, action) {
  switch (action.type) {
    case companyConstants.GET_REQUEST:
      return {
        ...state,
        isLoading: true,
        company: null,
        error: null
      };
    case companyConstants.GET_SUCCESS:
      return {
        ...state,
        isLoading: false,
        company: action.company
      };
    case companyConstants.GET_FAILURE:
      return {
        ...state,
        error: action.error,
        isLoading: false,
        company: null
      };
    case companyConstants.GET_COMPANY_ANALYTICS_REQUEST:
      return {
        ...state,
        isLoading: false,
        peopleAnalytics: null,
        error: null
      };
    case companyConstants.GET_COMPANY_ANALYTICS_SUCCESS:
      return {
        ...state,
        isLoading: false,
        peopleAnalytics: action.peopleAnalytics
      };
    case companyConstants.GET_COMPANY_ANALYTICS_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: action.error,
        peopleAnalytics: null
      };
    case companyConstants.GET_TOP_SKILLS_REQUEST:
      return {
        ...state,
        isLoadingTopSkills: true,
        companyTopSkills: null
      };
    case companyConstants.GET_TOP_SKILLS_SUCCESS:
      return {
        ...state,
        isLoadingTopSkills: false,
        companyTopSkills: action.companyTopSkills
      };
    case companyConstants.GET_TOP_SKILLS_FAILURE:
      return {
        ...state,
        isLoadingTopSkills: false,
        companyTopSkills: null
      };
    case companyConstants.GET_SKILLS_GAP_REQUEST:
      return {
        ...state,
        isLoadingSkillsGap: true,
        companySkillsGap: null
      };
    case companyConstants.GET_SKILLS_GAP_SUCCESS:
      return {
        ...state,
        isLoadingSkillsGap: false,
        companySkillsGap: action.companySkillsGap
      };
    case companyConstants.GET_SKILLS_GAP_FAILURE:
      return {
        ...state,
        isLoadingSkillsGap: false,
        companySkillsGap: null
      };
    case companyConstants.GET_EMPLOYEES_REQUEST:
      return {
        ...state,
        isLoadingEmployees: true,
        companyEmployees: null
      };
    case companyConstants.GET_EMPLOYEES_SUCCESS:
      return {
        ...state,
        isLoadingEmployees: false,
        companyEmployees: action.employees
      };
    case companyConstants.GET_EMPLOYEES_FAILURE:
      return {
        ...state,
        isLoadingEmployees: false,
        companyEemployees: null
      };
    case companyConstants.GET_PERSONAS_REQUEST:
      return {
        ...state,
        isLoadingPersonas: true,
        personas: null
      };
    case companyConstants.GET_PERSONAS_SUCCESS:
      return {
        ...state,
        isLoadingPersonas: false,
        personas: action.personas
      };
    case companyConstants.GET_PERSONAS_FAILURE:
      return {
        ...state,
        isLoadingPersonas: false,
        personas: null
      };
    case companyConstants.GET_INTOUCH_REQUEST:
      return {
        ...state,
        getInTouchLoading: true,
        getInTouchSuccess: null,
        getInTouchError: null
      };
    case companyConstants.GET_INTOUCH_SUCCESS:
      return {
        ...state,
        getInTouchLoading: false,
        getInTouchSuccess: true,
        getInTouchError: null
      };
    case companyConstants.GET_INTOUCH_FAILURE:
      return {
        ...state,
        getInTouchLoading: false,
        getInTouchSuccess: false,
        getInTouchError: action.error
      };
    case companyConstants.INVITE_MEMBERS_REQUEST:
      return {
        ...state,
        inviteMembersLoading: true,
        inviteMembersSuccess: null,
        inviteMembersError: null
      };
    case companyConstants.INVITE_MEMBERS_SUCCESS:
      return {
        ...state,
        inviteMembersLoading: false,
        inviteMembersSuccess: true,
        inviteMembersError: null
      };
    case companyConstants.INVITE_MEMBERS_FAILURE:
      return {
        ...state,
        inviteMembersLoading: false,
        inviteMembersSuccess: false,
        inviteMembersError: action.error
      };
    default:
      return state;
  }
}
