import { employeeConstants } from '../constants';

const INITIAL_STATE = {
  isLoading: true,
  employee: null,
  isLoadingSkills: true,
  employeeSkills: null,
  isLoadingSkillsGap: true,
  employeeSkillsGap: null,
  isLoadingSkillsRadar: true,
  skillsRadarData: null
};

export function employees(state = INITIAL_STATE, action) {
  switch (action.type) {
    case employeeConstants.GET_REQUEST:
      return {
        ...state,
        isLoading: true,
        employee: null
      };
    case employeeConstants.GET_SUCCESS:
      return {
        ...state,
        isLoading: false,
        employee: action.employee
      };
    case employeeConstants.GET_FAILURE:
      return {
        ...state,
        error: action.error,
        isLoading: false,
        employee: null
      };
    case employeeConstants.GET_SKILLS_REQUEST:
      return {
        ...state,
        isLoadingSkills: true,
        employeeSkills: null
      };
    case employeeConstants.GET_SKILLS_SUCCESS:
      return {
        ...state,
        isLoadingSkills: false,
        employeeSkills: action.employeeSkills
      };
    case employeeConstants.GET_SKILLS_FAILURE:
      return {
        ...state,
        error: action.error,
        isLoadingSkills: false,
        employeeSkills: null
      };
    case employeeConstants.GET_SKILLS_GAP_REQUEST:
      return {
        ...state,
        isLoadingSkillsGap: true,
        employeeSkillsGap: null
      };
    case employeeConstants.GET_SKILLS_GAP_SUCCESS:
      return {
        ...state,
        isLoadingSkillsGap: false,
        employeeSkillsGap: action.employeeSkillsGap
      };
    case employeeConstants.GET_SKILLS_GAP_FAILURE:
      return {
        ...state,
        error: action.error,
        isLoadingSkillsGap: false,
        employeeSkillsGap: null
      };
    case employeeConstants.GET_SKILLS_RADAR_REQUEST:
      return {
        ...state,
        isLoadingSkillsRadar: true,
        skillsRadarData: null
      };
    case employeeConstants.GET_SKILLS_RADAR_SUCCESS:
      return {
        ...state,
        isLoadingSkillsRadar: false,
        skillsRadarData: action.skillsRadarData
      };
    case employeeConstants.GET_SKILLS_RADAR_FAILURE:
      return {
        ...state,
        error: action.error,
        isLoadingSkillsRadar: false,
        skillsRadarData: null
      };
    default:
      return state;
  }
}
