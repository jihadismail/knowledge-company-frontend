import { userConstants } from '../constants';
import { adminConstants } from '../constants';

let user = JSON.parse(localStorage.getItem('admin'));
const initialState = user ? { loggedIn: true, user } : {};

export function authentication(state = initialState, action) {
  switch (action.type) {
    case userConstants.LOGIN_REQUEST:
      return {
        loggingIn: true
      };
    case userConstants.LOGIN_SUCCESS:
      return {
        loggedIn: true,
        user: action.user
      };
    case userConstants.LOGIN_FAILURE:
      return {};
    case userConstants.LOGOUT:
      return {};
    case userConstants.RESET_PASSWORD_REQUEST:
      return {
        resettingPassword: true,
        email: action.email
      };
    case userConstants.RESET_PASSWORD_SUCCESS:
      return {
        requestedResetPassword: true
      };
    case userConstants.RESET_PASSWORD_FAILURE:
      return {};
    case userConstants.SUBMIT_PASSWORD_REQUEST:
      return {
        resettingPassword: true
      };
    case userConstants.SUBMIT_PASSWORD_SUCCESS:
      return {
        requestedResetPassword: true
      };
    case userConstants.SUBMIT_PASSWORD_FAILURE:
      return {};
    case adminConstants.SET_PASSWORD_SUCCESS:
      localStorage.setItem('admin', JSON.stringify(action.admin));
      return {
        user: action.admin,
        loggedIn: true
      };
    case adminConstants.LOGIN_REQUEST:
      return {
        loggingIn: true
      };
    case adminConstants.LOGIN_SUCCESS:
      localStorage.setItem('admin', JSON.stringify(action.admin));
      return {
        loggedIn: true,
        user: action.admin
      };
    case adminConstants.LOGIN_FAILURE:
      return {};
    case adminConstants.LOGOUT:
      return {};
    default:
      return state;
  }
}
