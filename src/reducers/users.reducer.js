import { userConstants } from '../constants';

const INITIAL_STATE = { isLoading: true, user: undefined };

export function users(state = INITIAL_STATE, action) {
  switch (action.type) {
    case userConstants.GETALL_REQUEST:
      return {
        loading: true
      };
    case userConstants.GETALL_SUCCESS:
      return {
        items: action.users
      };
    case userConstants.GETALL_FAILURE:
      return {
        error: action.error
      };
    case userConstants.GET_REQUEST:
      return {
        isLoading: true
      };
    case userConstants.GET_SUCCESS:
      return {
        user: action.user
      };
    case userConstants.GET_FAILURE:
      return {
        error: action.error
      };
    case userConstants.UPDATE_REQUEST:
      return {
        success: false,
        error: undefined
      };
    case userConstants.UPDATE_SUCCESS:
      return {
        user: action.user,
        success: true
      };
    case userConstants.UPDATE_FAILURE:
      return {
        error: action.error,
        success: false
      };
    case userConstants.CHANGE_PASSWORD_REQUEST:
      return {
        success: false,
        error: undefined,
        user: state.user
      };
    case userConstants.CHANGE_PASSWORD_SUCCESS:
      return {
        success: true,
        user: state.user
      };
    case userConstants.CHANGE_PASSWORD_FAILURE:
      return {
        error: action.error,
        user: state.user
      };
    case userConstants.DELETE_REQUEST:
      // add 'deleting:true' property to user being deleted
      return {
        ...state,
        items: state.items.map(
          user => (user.id === action.id ? { ...user, deleting: true } : user)
        )
      };
    case userConstants.DELETE_SUCCESS:
      // remove deleted user from state
      return {
        items: state.items.filter(user => user.id !== action.id)
      };
    case userConstants.DELETE_FAILURE:
      // remove 'deleting:true' property and add 'deleteError:[error]' property to user
      return {
        ...state,
        items: state.items.map(user => {
          if (user.id === action.id) {
            // make copy of user without 'deleting:true' property
            const { deleting, ...userCopy } = user;
            // return copy of user with 'deleteError:[error]' property
            return { ...userCopy, deleteError: action.error };
          }

          return user;
        })
      };
    case userConstants.LOGOUT:
      return {};
    default:
      return state;
  }
}
