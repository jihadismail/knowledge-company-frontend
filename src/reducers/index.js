import { combineReducers } from 'redux';

import { authentication } from './authentication.reducer';
import { users } from './users.reducer';
import { companies } from './companies.reducer';
import { employees } from './employees.reducer';
import { alert } from './alert.reducer';
import { admins } from './admins.reducer';

const rootReducer = combineReducers({
  authentication,
  users,
  companies,
  alert,
  employees,
  admins
});

export default rootReducer;
