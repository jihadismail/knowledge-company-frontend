import { adminConstants } from '../constants';

const INITIAL_STATE = {
  isLoading: true,
  admin: null,
  error: null
};

export function admins(state = INITIAL_STATE, action) {
  switch (action.type) {
    case adminConstants.SET_PASSWORD_REQUEST:
      return {
        isLoading: true,
        admin: null,
        error: null
      };
    case adminConstants.SET_PASSWORD_SUCCESS:
      return {
        isLoading: false,
        admin: action.admin
      };
    case adminConstants.SET_PASSWORD_FAILURE:
      return {
        error: action.error,
        isLoading: false,
        admin: null
      };
    case adminConstants.LOGOUT:
      return { admin: null };
    default:
      return state;
  }
}
