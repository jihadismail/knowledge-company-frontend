export function initialChars(text) {
  if (text === '') return '';

  if (text.lenth === 1) return text.toUpperCase();

  let splitted = text.split(' ');
  if (splitted.length === 2) {
    return (
      splitted[0].charAt(0).toUpperCase() + splitted[1].charAt(0).toUpperCase()
    );
  } else {
    return (
      splitted[0].charAt(0).toUpperCase() + splitted[0].charAt(1).toUpperCase()
    );
  }
}

export function capitalize(string) {
  return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
}

export function titleize(string) {
  var string_array = string.split(' ');
  string_array = string_array.map(function(str) {
    return capitalize(str);
  });

  return string_array.join(' ');
}

export function truncate(text, truncationLimit) {
  let newText = text;
  if (text.length > truncationLimit) {
    newText = `${text.substring(0, truncationLimit)}...`;
  }
  return newText;
}

export function extractDomain(url) {
  return url
    .match(/^https?:\/\/([^/?#]+)(?:[/?#]|$)/i)[1]
    .split('.')
    .slice(-2)
    .join('.');
}
