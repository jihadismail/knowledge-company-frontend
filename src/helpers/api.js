export function apiUrl() {
  let base_url = '';
  switch (process.env.REACT_APP_ENV) {
    case 'production':
      base_url = 'https://api.Knowledgeofficer.com/business';
      break;
    case 'staging':
      base_url = 'https://staging-api.Knowledgeofficer.com/business';
      break;
    case 'development':
      base_url = 'https://staging-api.Knowledgeofficer.com/business';
      // base_url = 'http://localhost:3000/business';
      break;
    default:
      base_url = 'http://localhost:3000/business';
      break;
  }
  return base_url;
}

export function unversionedApiUrl() {
  let base_url = '';
  switch (process.env.REACT_APP_ENV) {
    case 'production':
      base_url = 'https://api.Knowledgeofficer.com';
      break;
    case 'staging':
      base_url = 'http://staging-api.Knowledgeofficer.com';
      break;
    case 'development':
      // base_url = 'http://127.0.0.1:3000'; // needed for linkedin
      base_url = 'http://staging-api.Knowledgeofficer.com';
      break;
    default:
      base_url = 'http://localhost:3000';
      break;
  }
  return base_url;
}

export function fetch_retry(url, options, n = 3) {
  return fetch(url, options)
    .then(function(response) {
      if (!response.ok) {
        if (response.status >= 500) {
          throw Error('Error connecting to server');
        }
      }
      return Promise.resolve(response);
    })
    .catch(function(error) {
      if (n === 1) throw error;
      return fetch_retry(url, options, n - 1);
    });
}

export function handleResponse(response) {
  if (!response.ok) {
    if (response.status >= 500) {
      throw Error('Server Error');
    }
    return Promise.reject(response.json());
  }

  return response.text().then(function(text) {
    return text.length ? JSON.parse(text) : {};
  });
}

export function handleConnetionError(error) {
  if (error instanceof Error) {
    return Promise.reject('Error connecting to server');
  } else {
    return error.then(errorsmsgs => {
      let err = '';
      if (
        typeof errorsmsgs.errors === 'string' ||
        errorsmsgs.errors instanceof String
      ) {
        err = errorsmsgs.errors;
      } else if (errorsmsgs.error) {
        err = errorsmsgs.error;
      } else {
        err = Object.entries(errorsmsgs.errors)[0].join(' ');
      }
      return Promise.reject(err);
    });
  }
}
