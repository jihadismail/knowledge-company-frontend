import React from 'react';
import { Link } from 'react-router-dom';

class CookiePolicyPage extends React.Component {
  render() {
    return (
      <div id="cookiePolicyPage" className="policy-page">
        <div className="col-md-6 offset-md-3">
          <h1>Cookie Policy</h1>
          <h2>1 Who we are</h2>
          <p>
            We are Knowledge Officer Ltd (<b>Knowledge Officer</b>), a company
            incorporated in England &amp; Wales with registered number 10330450
            and having our registered office address at 107 Firs Close, Mitcham,
            United Kingdom, CR4 1AX.
          </p>
          <p>
            We are a <b>data controller</b> for the purposes of the General Data
            Protection Regulation (Regulation (EU) 2016/679) and related data
            protection legislation.
          </p>

          <h2>2 Cookie Policy</h2>
          <p>This Cookie Policy was last updated on 24 May 2018.</p>

          <h2>3 Information about our use of cookies</h2>
          <p>
            Our website (<Link to="/" target="_blank">
              {' '}
              www.knowledgeofficer.com{' '}
            </Link>{' '}
            or any other domain name registered in our name) and our
            mobile/desktop application (<b>App</b>) uses cookies to distinguish
            you from other users of our website/App. This helps us to provide
            you with a good experience when you browse our website and use our
            App and also allows us to improve our site and App. By continuing to
            browse the site (or use our App), you are agreeing to our use of
            cookies.
          </p>
          <p>
            A cookie is a small file of letters and numbers that we store on
            your browser or the hard drive of your computer if you agree.
            Cookies contain information that is transferred to your computer’s
            hard drive.
          </p>
          <p>
            We use the following cookies:
            <ul>
              <li>
                <b>Strictly necessary cookies.</b> These are cookies that are
                required for the operation of our website/App. They include, for
                example, cookies that enable you to log into secure areas of our
                website/App, use a shopping cart or make use of e-billing
                services.
              </li>
              <li>
                <b>Analytical/performance cookies.</b> They allow us to
                recognise and count the number of visitors and to see how
                visitors move around our website/App when they are using it.
                This helps us to improve the way our website works, for example,
                by ensuring that users are finding what they are looking for
                easily.
              </li>
              <li>
                <b>Functionality cookies.</b> These are used to recognise you
                when you return to our website/App. This enables us to
                personalise our content for you, greet you by name and remember
                your preferences (for example, your choice of language or
                region).
              </li>
              <li>
                <b>Targeting cookies.</b> These cookies record your visit to our
                website/App, the pages you have visited and the links you have
                followed. We will use this information to make our website and
                the advertising displayed on it more relevant to your interests.
                We may also share this information with third parties for this
                purpose.
              </li>
            </ul>
            <p>
              Please note that third parties (including, for example,
              advertising networks and providers of external services like web
              traffic analysis services) may also use cookies, over which we
              have no control. These cookies are likely to be
              analytical/performance cookies or targeting cookies.
            </p>
          </p>

          <h2>4 Managing cookies</h2>
          <p>
            You can block cookies by activating the setting on your browser that
            allows you to refuse the setting of all or some cookies. However, if
            you use your browser settings to block all cookies (including
            essential cookies) you may not be able to access all or parts of our
            site.
          </p>

          <h2>5 Other Information</h2>
          <p>
            We also use analytic services to help us understand how effective
            our content is, what interests our users have, and to improve how
            this website works. In addition, we use web beacons or tracking
            pixels to count visitor number and performance cookies to track how
            many individual users access this website and how often.
          </p>
          <p>
            This information is used for statistical purposes only and we do not
            use such information to personally identify any user.
          </p>

          <h2>6 Data Protection</h2>
          <p>
            For more information, please see our{' '}
            <Link to="/privacy-policy" target="_top">
              Privacy Policy.
            </Link>
          </p>
        </div>
      </div>
    );
  }
}

export { CookiePolicyPage };
