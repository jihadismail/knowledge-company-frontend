import React from 'react';
import { Link } from 'react-router-dom';

class PrivacyPolicyPage extends React.Component {
  render() {
    const tableStyle = {
      border: '1px solid black'
    };
    const thStyle = {
      border: '1px solid black',
      padding: '10px',
      'text-align': 'center'
    };

    const tdStyle = {
      border: '1px solid black',
      padding: '10px',
      'text-align': 'left'
    };

    return (
      <div id="privacyPolicyPage" className="policy-page">
        <div className="col-md-6 offset-md-3">
          <h1>Privacy Policy</h1>
          <h2>1 Who we are</h2>
          <p>
            <p>
              We are Knowledge Officer Ltd (<b>Knowledge Officer</b>), a company
              incorporated in England &amp; Wales with registered number
              10330450 and having our registered office address at 107 Firs
              Close, Mitcham, United Kingdom, CR4 1AX.
            </p>
            <p>
              We are a <b>data controller</b> for the purposes of the General
              Data Protection Regulation (Regulation (EU) 2016/679) and related
              data protection legislation.
            </p>
          </p>

          <h2>2 How to contact us</h2>
          <p>
            If you have any questions about this privacy notice, including any
            requests to exercise your legal rights, please contact us using the
            details set out below.
            <ul>
              <li>By post: 107 Firs Close, Mitcham, United Kingdom, CR4 1AX</li>
              <li>
                By email:{' '}
                <a href="mailto:team@knowledgeofficer.com">
                  team@knowledgeofficer.com
                </a>
              </li>
            </ul>
          </p>

          <h2>3 Privacy Information</h2>
          <p>
            <p>
              We are committed to protecting your personal data and your
              privacy. This privacy notice aims to give you information on how
              we collect and process your personal data through your use of our
              website or our App (including any data that you may provide
              through our website or our App) and through your other
              communications with us.
            </p>
            <p>
              It is important that you read this privacy notice together with
              any other privacy notice or fair processing notice we may provide
              on specific occasions when we are collecting or processing
              personal data about you so that you are fully aware of how and why
              we are using your data. This privacy notice supplements the other
              notices and is not intended to override them.
            </p>
            <p>
              It is important that the personal data we hold about you is
              accurate and current. Please keep us informed if your personal
              data changes during your relationship with us.
            </p>
            This version of our privacy notice was last updated on 24 May 2018.
          </p>

          <h2>4 To whom does this privacy notice apply?</h2>
          <p>
            This privacy notice applies to all individuals who visit our
            website, who download our mobile/desktop application (our <b>App</b>)
            or who contact us by post, telephone, e-mail or other means
            (including other electronic means).
          </p>

          <h2>5 Information about our website and App</h2>
          <p>
            <p>
              Our website (as hosted on{' '}
              <Link to="/">www.knowledgeofficer.com</Link> or any other domain
              name registered in our name) and{' '}
              <a
                href="https://itunes.apple.com/us/app/knowledge-officer/id1327830032"
                target="_blank"
                rel="noopener noreferrer"
              >
                {' '}
                App{' '}
              </a>
              may include links to third-party websites, plug-ins and
              applications. Clicking on those links or enabling those
              connections may allow third parties to collect or share data about
              you. We do not control these third-party websites and are not
              responsible for their privacy statements. When you leave our
              website, we encourage you to read the privacy notice of every
              website you visit.
            </p>
            <p>
              Cookies are small text files that are placed on your computer by
              websites that you visit. They are widely used in order to make
              websites work, or work more efficiently, as well as to provide
              information to the owners of the site.
            </p>

            <p>
              You can set your browser to refuse all or some browser cookies, or
              to alert you when websites set or access cookies. If you disable
              or refuse cookies, please note that some parts of the Website may
              become inaccessible or not function properly.
            </p>
          </p>

          <h2>6 Children</h2>
          <p>
            Our website and App are not intended for children and we do not
            knowingly collect any data relating to children.
          </p>

          <h2>7 What personal data do we collect and process?</h2>
          <p>
            <p>
              <b>Personal data</b>, or personal information, means any
              information about an individual from which that person can be
              identified. It does not include data where the identity has been
              removed (<b>anonymous data</b>).
            </p>
            We may collect, use, store and transfer different kinds of personal
            data about you which we have grouped together follows:
            <ul>
              <li>
                <b>Identity Data</b> includes first name, last name, social
                media username (in particular LinkedIn or email+ information) or
                similar identifier, marital status, title, date of birth and
                gender.
              </li>
              <li>
                <b>Contact Data</b> includes home or work address, email address
                and telephone numbers.
              </li>
              <li>
                <b>Financial Data</b> includes bank account and payment card
                details.
              </li>
              <li>
                <b>Technical Data</b> includes internet protocol (IP) address,
                your login data, browser type and version, time zone setting and
                location, browser plug-in types and versions, operating system
                and platform and other technology on the devices you use to
                access our website or App.
              </li>
              <li>
                <b>Profile Data</b> includes your username and password,
                purchases or orders made by you, your interests, preferences,
                feedback and survey responses, and, where you have linked your
                account with us with a social media account, your social media
                profile.
              </li>
              <li>
                <b>Usage Data</b> includes information about how you use our
                website and App, including your scores in challenges and other
                progress reports.
              </li>
              <li>
                <b>Transaction Data</b> includes information about App downloads
                and other products pr services you purchase from us.
              </li>
              <li>
                <b>Marketing and Communications Data</b> includes your
                preferences in receiving marketing from us and your
                communication preferences.
              </li>
            </ul>
            <p>
              We also collect, use and share <b>Aggregated Data</b> such as
              statistical or demographic data for any purpose. Aggregated Data
              may be derived from your personal data but is not considered
              personal data in law as this data does not directly or indirectly
              reveal your identity. For example, we may aggregate your Usage
              Data to calculate the percentage of users accessing a specific
              website or App feature. However, if we combine or connect
              Aggregated Data with your personal data so that it can directly or
              indirectly identify you, we treat the combined data as personal
              data which will be used in accordance with this privacy notice.
            </p>
            <p>
              We do not collect any <b>"special categories of personal data"</b>{' '}
              about you (this includes details about your race or ethnicity,
              religious or philosophical beliefs, sex life, sexual orientation,
              political opinions, trade union membership, information about your
              health and genetic and biometric data). Nor do we collect any
              information about criminal convictions and offences.
            </p>
          </p>

          <h2>8 How is your personal data obtained?</h2>
          <p>
            <p>
              We use different methods to collect data from and about you
              including through:
            </p>
            <ul>
              <li>
                <b>Direct interactions.</b> You may give us your Identity,
                Contact and Financial Data by filling in forms or by
                corresponding with us by post, phone, email, via social media or
                otherwise. You may give us your Profile or Usage Data by using
                our website and App This includes personal data you provide when
                you:
                <ul>
                  <li>download and install our App;</li>
                  <li>create an account on our website or App;</li>
                  <li>request marketing to be sent to you;</li>
                  <li>enter a competition, promotion or survey; or</li>
                  <li>give us some feedback.</li>
                </ul>
              </li>
              <li>
                <b>Automated technologies or interactions.</b> As you interact
                with our website, we may automatically collect Technical Data
                about your equipment, browsing actions and patterns. We collect
                this personal data by using cookies, server logs and other
                similar technologies. Please see our{' '}
                <Link to="/cookie-policy" target="_top">
                  Cookie Policy{' '}
                </Link>{' '}
                for further details.
              </li>

              <li>
                <b>Third parties or publicly available sources.</b> We may
                receive personal data about you from various third parties and
                public sources as set out:
                <ul>
                  <li>
                    Technical Data from analytics providers, advertising
                    networks or search information providers (such as Google
                    Analytics).
                  </li>
                  <li>
                    Contact, Financial and Transaction Data from providers of
                    technical, payment and delivery services.
                  </li>
                  <li>
                    Identity, Contact and Profile Data from social media
                    providers (such as LinkedIn or email+) where you use these
                    to correspond with us.
                  </li>
                </ul>
              </li>
            </ul>
          </p>

          <h2>9 Failure to provide personal data</h2>
          <p>
            Where we need to collect personal data by law, or under the terms of
            a contract we have with you and you fail to provide that data when
            requested, we may not be able to perform the contract we have or are
            trying to enter into with you (for example, to provide you with
            goods or services). In this case, we may have to cancel a product or
            service you have with us but we will notify you if this is the case
            at the time.
          </p>

          <h2>10 How will we use your personal data?</h2>
          <p>
            Personal data will be processed by us where you consent to the
            processing or where that processing is necessary for
            <ol>
              <li>the performance of a contract with you; or</li>
              <li>
                compliance with a legal obligation to which we are subject; or
              </li>
              <li>
                the purposes of our legitimate interests (or those of a third
                party).
              </li>
            </ol>
          </p>
          <p>
            We have set out below, in a table format, a description of all the
            ways we plan to use your personal data, and which of the legal bases
            we rely on to do so. We have also identified what our legitimate
            interests are where appropriate.
          </p>
          <p>
            <p>
              Note that we may process your personal data for more than one
              lawful ground depending on the specific purpose for which we are
              using your data. Please{' '}
              <a href="mailto:team@knowledgeofficer.com">Contact us</a> if you
              need details about the specific legal ground we are relying on to
              process your personal data where more than one ground has been set
              out in the table below.
            </p>
            <table style={tableStyle}>
              <tr>
                <th width="40%" style={thStyle}>
                  Purpose/Activity
                </th>
                <th width="10%" style={thStyle}>
                  Type of data
                </th>
                <th width="50%" style={thStyle}>
                  Lawful basis for processing including basis of legitimate
                  interest
                </th>
              </tr>
              <tr>
                <td style={tdStyle}>
                  To register you as a new customer and to administer such
                  online account with us
                </td>
                <td style={tdStyle}>
                  <ol type="a">
                    <li>Identity</li>
                    <li>Contact</li>
                  </ol>
                </td>
                <td style={tdStyle}>Performance of a contract with you</td>
              </tr>
              <tr>
                <td style={tdStyle}>
                  To process and deliver your order / download including:
                  <ol type="a">
                    <li>Manage payments, fees and charges</li>
                    <li>Collect and recover money owed to us</li>
                    <li>Deal with any customer complaints </li>
                  </ol>
                </td>
                <td style={tdStyle}>
                  <ol type="a">
                    <li>Identity</li>
                    <li>Contact</li>
                    <li>Financial</li>
                    <li>Transaction</li>
                    <li>Marketing and Communications</li>
                  </ol>
                </td>
                <td style={tdStyle}>
                  <ol type="a">
                    <li>Performance of a contract with you</li>
                    <li>
                      Necessary for our legitimate interests (to recover debts
                      due to us and to manage customer relationships)
                    </li>
                    <li>Necessary to comply with a legal obligation</li>
                  </ol>
                </td>
              </tr>
              <tr>
                <td style={tdStyle}>
                  To manage our relationship with you, including:
                  <ol type="a">
                    <li>
                      providing knowledge development services via our App;
                    </li>
                    <li>
                      notifying you about changes to our terms and conditions or
                      our privacy policy
                    </li>
                  </ol>
                </td>
                <td style={tdStyle}>
                  <ol type="a">
                    <li>Identity</li>
                    <li>Contact</li>
                    <li>Profile</li>
                    <li>Marketing and Communications</li>
                  </ol>
                </td>
                <td style={tdStyle}>
                  <ol type="a">
                    <li>Performance of a contract with you</li>
                    <li>Necessary to comply with a legal obligation</li>
                  </ol>
                </td>
              </tr>
              <tr>
                <td style={tdStyle}>
                  To administer and protect our business, website and App
                  (including troubleshooting, data analysis, testing, system
                  maintenance, support, reporting and hosting of data)
                </td>
                <td style={tdStyle}>
                  <ol type="a">
                    <li>Identity</li>
                    <li>Contact</li>
                    <li>Technical</li>
                  </ol>
                </td>
                <td style={tdStyle}>
                  <ol type="a">
                    <li>
                      Necessary for our legitimate interests (for running our
                      business, provision of administration and IT services,
                      network security)
                    </li>
                    <li>Necessary to comply with a legal obligation</li>
                  </ol>
                </td>
              </tr>
              <tr>
                <td style={tdStyle}>
                  To use data analytics to improve our website, App, marketing,
                  customer relationships and experiences
                </td>
                <td style={tdStyle}>
                  <ol type="a">
                    <li>Technical</li>
                    <li>Usage</li>
                  </ol>
                </td>
                <td style={tdStyle}>
                  Necessary for our legitimate interests (to define types of
                  customers for our products and services, to keep our website
                  updated and relevant, to develop our business and to inform
                  our marketing strategy)
                </td>
              </tr>
              <tr>
                <td style={tdStyle}>
                  To establish, exercise and defend our legal rights
                </td>
                <td style={tdStyle}>All data</td>
                <td style={tdStyle}>
                  Necessary for our legitimate interests (in protecting our
                  legal rights)
                </td>
              </tr>
            </table>
            <br />
            <p>
              We will only use your personal data for the purposes for which we
              collected it, unless we reasonably consider that we need to use it
              for another reason and that reason is compatible with the original
              purpose. If you wish to get an explanation as to how the
              processing for the new purpose is compatible with the original
              purpose, please Contact us.
            </p>
            <p>
              If we need to use your personal data for an unrelated purpose, we
              will notify you and we will explain the legal basis which allows
              us to do so.
            </p>
          </p>

          <h2>11 Marketing</h2>
          <p>
            Our direct marketing communications generally consist of delivering
            regular newsletters and updates on our business, usually by e-mail
            or other electronic means.
          </p>
          <p>
            We will only provide you with direct marketing communications where
            you have consented to receive such communications. You can subscribe
            to such marketing communications, and you can adjust your marketing
            preferences at any time via the App or by contacting us on
            <a href="mailto:team@knowledgeofficer.com">
              {' '}
              team@knowledgeofficer.com
            </a>.
          </p>
          <p>
            You can also opt-out or unsubscribe from all or some of these
            marketing communications at any time via the App, by contacting us
            on{' '}
            <a href="mailto:team@knowledgeofficer.com">
              {' '}
              team@knowledgeofficer.com
            </a>{' '}
            or by clicking “unsubscribe” at the bottom of any marketing e-mail.
          </p>
          <p>
            Where you opt out of receiving these marketing communications, this
            opt-out will not apply to personal data provided to us for any other
            purpose.
          </p>

          <h2>12 With whom do we share your personal data?</h2>
          <p>
            <p>
              We may have to share your personal data with the parties set out
              below for the purposes set out in the table above.
            </p>
            <ul>
              <li>Payment and credit card providers.</li>
              <li>
                Service providers acting as processors who provide IT and system
                administration services, including Google Analytics, Woopra,
                Linode and Amazon.
              </li>
              <li>
                Professional advisers including lawyers, bankers, auditors and
                insurers who provide consultancy, banking, legal, insurance and
                accounting services.
              </li>
              <li>
                Any relevant regulatory authority or law enforcement agency,
                including HM Revenue & Customs, Trading Standards, Advertising
                Standards Authority, courts or tribunals who require reporting
                of processing activities in certain circumstances.
              </li>
              <li>
                Third parties to whom we may choose to sell, transfer, or merge
                parts of our business or our assets. Alternatively, we may seek
                to acquire other businesses or merge with them. If a change
                happens to our business, then the new owners may use your
                personal data in the same way as set out in this privacy notice.
              </li>
            </ul>
            <p>
              We require all third parties to respect the security of your
              personal data and to treat it in accordance with the law. We do
              not allow our third-party service providers to use your personal
              data for their own purposes and only permit them to process your
              personal data for specified purposes and in accordance with our
              instructions.
            </p>
            <p>
              Any sharing of your personal data will only take place either
              where we are legally obliged to do so, where it is necessary for
              the performance of a contract with you or where it is in our
              legitimate interests to do so, including as follows:
              <ul>
                <li>to maintain network and information security;</li>
                <li>
                  to develop and improve our services and products in order to
                  remain competitive;
                </li>
                <li>to protect and defend our legal rights;</li>
                <li>
                  to pursue our commercial objectives where this does not
                  override your rights and freedoms as a data subject.
                </li>
              </ul>
            </p>
          </p>

          <h2>13 International transfers</h2>
          <p>
            Some of our external third parties (such as Google Analytics and
            Amazon) are headquartered or based outside of the EEA so their
            processing of your personal data will involve a transfer of data
            outside of the EEA.
          </p>

          <p>
            Whenever we transfer your personal data out of the EEA, we ensure a
            similar degree of protection is afforded to it by ensuring at least
            one of the following safeguards is implemented:
            <ul>
              <li>
                We will only transfer your personal data to countries that have
                been deemed to provide an adequate level of protection for
                personal data by the European Commission.
              </li>
              <li>
                Where we use certain service providers, we may use specific
                contracts approved by the European Commission which give
                personal data the same protection it has in Europe.
              </li>
              <li>
                Where we use providers based in the USA (such as Google and
                Amazon), we may transfer data to them if they are part of the
                Privacy Shield which requires them to provide similar protection
                to personal data shared between the European Union and the USA.
              </li>
            </ul>
          </p>

          <h2>14 Automated decision making and profiling</h2>
          <p>
            We do not use automated decision-making (including profiling) to
            make any decisions which would produce a legal effect or similarly
            significantly affect a data subject.
          </p>

          <h2>15 How long do we retain your personal data?</h2>
          <p>
            We will only retain your personal data for as long as necessary to
            fulfil the purposes we collected it for, including for the purposes
            of satisfying any legal, accounting, or reporting requirements.
          </p>
          <p>
            To determine the appropriate retention period for personal data, we
            consider the amount, nature, and sensitivity of the personal data,
            the potential risk of harm from unauthorised use or disclosure of
            your personal data, the purposes for which we process your personal
            data and whether we can achieve those purposes through other means,
            and the applicable legal requirements.
          </p>
          <p>
            For tax purposes, we retain basic information about our customers
            (including Contact, Identity, Financial and Transaction Data) for
            six years after they cease being customers.
          </p>

          <h2>16 Data security</h2>
          <p>
            We have put in place appropriate security measures to prevent your
            personal data from being accidentally lost, used or accessed in an
            unauthorised way, altered or disclosed. In addition, we limit access
            to your personal data to those employees, agents, contractors and
            other third parties who have a business need to know. They will only
            process your personal data on our instructions and they are subject
            to a duty of confidentiality.
          </p>
          <p>
            We have put in place procedures to deal with any suspected personal
            data breach and will notify you and the Information Commissioner's
            Office of a breach where we are legally required to do so.
          </p>

          <h2>17 Your rights</h2>
          <ol>
            <li>
              Your personal data is protected by legal rights, which include
              your rights to:
              <ul>
                <li>
                  <b>Request access to your personal data</b> (commonly known as
                  a "data subject access request"). This enables you to receive
                  a copy of the personal data we hold about you and to check
                  that we are lawfully processing it.
                </li>
                <li>
                  <b>
                    Request correction of the personal data that we hold about
                    you.
                  </b>{' '}
                  This enables you to have any incomplete or inaccurate data we
                  hold about you corrected, though we may need to verify the
                  accuracy of the new data you provide to us.
                </li>
                <li>
                  <b>Request erasure of your personal data.</b> This enables you
                  to ask us to delete or remove personal data where there is no
                  good reason for us continuing to process it. Note, however,
                  that we may not always be able to comply with your request of
                  erasure for specific legal reasons which will be notified to
                  you, if applicable, following your request.
                </li>
                <li>
                  <b>Object to processing of your personal data </b> where we
                  are relying on a legitimate interest (or those of a third
                  party) and there is something about your particular situation
                  which makes you want to object to processing on this ground as
                  you feel it impacts on your fundamental rights and freedoms.
                  You also have the right to object where we are processing your
                  personal data for direct marketing purposes.
                </li>
                <li>
                  <b>
                    Request restriction of processing of your personal data.{' '}
                  </b>This enables you to ask us to suspend the processing of
                  your personal data in the following scenarios: if you want us
                  to establish the data's accuracy; where our use of the data is
                  unlawful but you do not want us to erase it; where you need us
                  to hold the data even if we no longer require it as you need
                  it to establish, exercise or defend legal claims; or you have
                  objected to our use of your data but we need to verify whether
                  we have overriding legitimate grounds to use it.
                </li>
                <li>
                  <b>
                    Request the transfer of your personal data to you or to a
                    third party (data portability).
                  </b>{' '}
                  We will provide to you, or a third party you have chosen, your
                  personal data in a structured, commonly used, machine-readable
                  format. Note that this right only applies to automated
                  information which you initially provided consent for us to use
                  or where we used the information to perform a contract with
                  you.
                </li>
                <li>
                  <b>
                    Withdraw consent at any time where we are relying on consent
                    to process your personal data.
                  </b>{' '}
                  However, this will not affect the lawfulness of any processing
                  carried out before you withdraw your consent. If you withdraw
                  your consent, we may not be able to provide certain products
                  or services to you. We will advise you if this is the case at
                  the time you withdraw your consent.
                </li>
              </ul>
            </li>
            <li>
              If you wish to exercise any of these rights, please contact us
              using the details above.
            </li>
            <li>
              We may need to request specific information from you to help us
              confirm your identity and ensure your right to access your
              personal data (or to exercise any of your other rights). This is a
              security measure to ensure that personal data is not disclosed to
              any person who has no right to receive it. We may also contact you
              to ask you for further information in relation to your request to
              speed up our response.
            </li>

            <li>
              You will not have to pay a fee to access your personal data (or to
              exercise any of the other rights). However, we may charge a
              reasonable fee if your request is clearly unfounded, repetitive or
              excessive. Alternatively, we may refuse to comply with your
              request in these circumstances.
            </li>

            <li>
              We try to respond to all legitimate requests within one month.
              Occasionally it may take us longer than a month if your request is
              particularly complex or you have made a number of requests. In
              this case, we will notify you and keep you updated.
            </li>

            <li>
              You also have the right to complain to the Information
              Commissioner's Office, which regulates the processing of personal
              data, about how we are processing your personal data.
            </li>
          </ol>
        </div>
      </div>
    );
  }
}

export { PrivacyPolicyPage };
