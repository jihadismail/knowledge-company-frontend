import React, { Fragment } from 'react';
import { GradientScreen, CharacterBlock } from 'components';

import './GetInTouchBlock.css';

class GetInTouchBlock extends React.Component {
  handleClickGetInTouch = () => {
    this.props.handleClickGetInTouch();
  };

  render() {
    return (
      <Fragment>
        <GradientScreen
          showCloseButton={false}
          showSecondayDismissButton={false}
          dismissButtonText={''}
          dismissButtonColor={'dark'}
          onDismiss={this.handleDismissGradientScreen}
          screenClass={'get-in-touch'}
        >
          <CharacterBlock
            speechBubbleMessage={
              'Want to find out more? We can evaluate skills across your entire company and then assign employees customised learning paths to fill your skills gaps and level up your work force.'
            }
            characterClasses={'character all-characters'}
            showHill={false}
            showLogo={false}
          />
          <div className="action-btn-wrapper">
            <button
              className="ko-btn-primary wihout-shadow"
              onClick={this.handleClickGetInTouch}
            >
              Get in touch
            </button>
          </div>
        </GradientScreen>
      </Fragment>
    );
  }
}

export { GetInTouchBlock };
