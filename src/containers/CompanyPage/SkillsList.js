import React from 'react';
import { TopSkillsList } from './TopSkillsList';
import { SkillsGapList } from './SkillsGapList';

class SkillsList extends React.Component {
  handleMore = e => {
    this.props.handleMore();
  };

  handleClickGetInTouch = () => {
    this.props.handleClickGetInTouch();
  };

  render() {
    return (
      <section className="content-wrapper skills-list">
        <TopSkillsList
          companyid={this.props.companyid}
          companyActive={this.props.companyActive}
        />
        <SkillsGapList
          companyid={this.props.companyid}
          companyActive={this.props.companyActive}
        />
        <button
          className="ko-action-link view-all-skills"
          onClick={this.handleClickGetInTouch}
          // onClick={this.handleMore}
        >
          {this.props.showViewAllSkills ? '+ view all skills' : ' '}
        </button>
      </section>
    );
  }
}

export { SkillsList };
