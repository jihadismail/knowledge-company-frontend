import React from 'react';
import { B2BPeopleInsights } from './B2BInsightsReportCategories';

import './CompanyPage.css';

class B2BInsightsReport extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      // a list contains the report categories' components name
      categories: [{ type: 'People', component: B2BPeopleInsights }],
      selectedCategory: {} // by default the first category component is selected
    };
  }
  componentDidMount() {}

  //Actions
  handleCategoryOnClick = category => {
    this.setState({
      selectedCategory: category
    });
    document.getElementById('button').style.backgroundColor = '#f7f7f7';
  };

  // Renders
  renderSelectedCategory = () => {
    console.log('called');
    const category = this.state.selectedCategory;
    console.log('component: ' + category.component);
    if (!category.component) return <div />;

    const Component = category.component;

    return <Component analyticsId={this.props.analyticsId} />;
  };

  renderB2BInsightsCategories() {
    return (
      <div className="main-categories_btns">
        <button className="locked-buttons">Overview</button>
        {// loop on all the categories and create a button with it
        this.state.categories.map(category => {
          return (
            <button
              id="button"
              onClick={() => {
                this.handleCategoryOnClick(category);
              }}
            >
              {' '}
              {category.type}{' '}
            </button>
          );
        })}
        <button className="locked-buttons">Jobs</button>
        <button className="locked-buttons">Competative Insights</button>
      </div>
    );
  }

  render() {
    return (
      <div>
        {this.renderB2BInsightsCategories()}
        {this.renderSelectedCategory()}
      </div>
    );
  }
}

export { B2BInsightsReport };
