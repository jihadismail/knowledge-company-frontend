import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { EmployeesLists } from './EmployeesLists';
import { SkillsList } from './SkillsList';
import { InviteEmployeeModal } from 'components';
import { GetInTouchBlock } from './GetInTouchBlock';
import { GetInTouchModal } from './GetInTouchModal';
import { Loading, CharacterCircle } from 'components';
import { companyActions } from 'actions';
import { history } from 'helpers';

import { B2BInsightsReport } from './B2BInsightsReport';

import company_logo from './images/company-logo.png';
import link_icon from './images/link-icon.svg';

import './CompanyPage.css';

class CompanyPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      inviteEmployeeModalIsVisible: false,
      getInTouchIsVisible: false,
      showGetInTouch: false,
      companyActive: false,
      isB2BInsightsOpen: false
    };
  }

  componentDidMount() {
    this.props.dispatch(companyActions.get(this.props.match.params.id));
  }

  handleClickInviteEmployees = () => {
    this.setState({
      inviteEmployeeModalIsVisible: true
    });
  };

  handleHideInvitationModal = () => {
    this.setState({
      inviteEmployeeModalIsVisible: false
    });
  };

  handleClickGetInTouch = () => {
    this.setState({
      getInTouchIsVisible: true
    });
  };

  handleHideGetInTouch = () => {
    this.setState({
      getInTouchIsVisible: false
    });
  };

  handleMore = () => {
    if (!this.state.companyActive) {
      this.setState({
        getInTouchIsVisible: true
      });
    }
  };

  handleInvite = () => {
    if (this.state.companyActive) {
      this.setState({
        inviteEmployeeModalIsVisible: true
      });
    }
  };

  handleB2BInsightsClick = () => {
    this.setState({
      isB2BInsightsOpen: true
    });
  };

  handleCompanyOldInsightsClick = () => {
    this.setState({
      isB2BInsightsOpen: false
    });
  };

  handleCompareSearch = () => {
    let search = document.getElementById('compareSearch');
    search.style.display = 'block';
    document.getElementById('compareButton').innerHTML = 'Search';
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.error === 'Access denied') {
      history.push('/login');
    }
    if (nextProps.company) {
      localStorage.setItem(
        'companyId',
        JSON.stringify(nextProps.company.permalink)
      );
      this.setState({ companyActive: nextProps.company.active });
    }
  }

  render() {
    const { inviteEmployeeModalIsVisible, getInTouchIsVisible } = this.state;
    const { isLoading, company, loggedIn } = this.props;

    let inviteEmployeeModal = null;
    if (inviteEmployeeModalIsVisible) {
      inviteEmployeeModal = (
        <InviteEmployeeModal
          handleHideInvitationModal={this.handleHideInvitationModal}
        />
      );
    }

    let inviteGetinTouchSection = null;
    if (this.state.companyActive) {
      inviteGetinTouchSection = (
        <article className="invite-employees-message center">
          <p className="opacity-5">
            Invite members of your team to improve your company data and help
            them boost their skills.
          </p>
          <button
            className="ko-btn-primary"
            onClick={this.handleClickInviteEmployees}
          >
            Invite employees
          </button>
        </article>
      );
    } else if (company) {
      inviteGetinTouchSection = (
        <GetInTouchBlock
          companyid={company.id}
          handleClickGetInTouch={this.handleClickGetInTouch}
          showModal={this.state.showGetInTouch}
        />
      );
    }

    return (
      <div id="companyProfile" className="company-page">
        {isLoading ? (
          <Loading inverted={true} />
        ) : company ? (
          <Fragment>
            {inviteEmployeeModal}
            <section className="page-insight">
              <div className="page-insight__wrapper">
                <div className="company-header">
                  <div className="company-info">
                    <img className="company-logo" src={company_logo} />
                    <h1 className="title">
                      {company.name}
                      <img className="link-icon" src={link_icon} />
                    </h1>
                    <input
                      id="compareSearch"
                      className="comapre-search"
                      type="text"
                      placeholder="Search.."
                      name="search"
                    />
                    <button
                      onClick={e => {
                        this.handleCompareSearch();
                      }}
                      id="compareButton"
                      className="comapre-button"
                    >
                      + Compare
                    </button>
                  </div>
                  <div>
                    <input type="text" placeholder="Search.." name="search" />
                    <button className="comapre-button">Search</button>
                  </div>
                </div>
                <button
                  onClick={e => {
                    this.handleCompanyOldInsightsClick();
                  }}
                >
                  {' '}
                  Old Company Insights{' '}
                </button>
                <button
                  onClick={e => {
                    this.handleB2BInsightsClick();
                  }}
                >
                  {' '}
                  New B2B Insights{' '}
                </button>
              </div>
              <hr />
            </section>
            {this.state.isB2BInsightsOpen ? (
              <B2BInsightsReport analyticsId={company.analytics_id} />
            ) : (
              <div>
                <section className="character-message">
                  <CharacterCircle
                    compactSize={true}
                    avatarSize={'c-90'}
                    backgroundColor={'light'}
                    speechBubbleMessage={
                      'Welcome to your company report! We’ve analysed your employees to discover your top company skills and find the skills you’re missing.'
                    }
                    characterClass={'circle-character-man'}
                  />
                </section>

                <SkillsList
                  companyid={company.id}
                  handleClickGetInTouch={this.handleClickGetInTouch}
                  handleMore={this.handleMore}
                  showViewAllSkills={this.state.companyActive ? false : true}
                  companyActive={this.state.companyActive}
                />
                <EmployeesLists
                  companyid={company.id}
                  companyPermalink={company.permalink}
                  handleClickGetInTouch={this.handleClickGetInTouch}
                  handleMore={this.handleMore}
                  handleInvite={this.handleInvite}
                  companyActive={this.state.companyActive}
                />
                {inviteGetinTouchSection}
                {getInTouchIsVisible && (
                  <GetInTouchModal
                    companyid={company.id}
                    handleClickGetInTouch={this.handleClickGetInTouch}
                    handleHideGetInTouch={this.handleHideGetInTouch}
                  />
                )}
              </div>
            )}
          </Fragment>
        ) : (
          <section className="page-insight">
            <div className="page-insight__wrapper">
              <h1 className="title">Company Not Found</h1>
            </div>
          </section>
        )}
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { companies, authentication } = state;
  const { isLoading, company, error } = companies;
  const { loggedIn } = authentication;
  return {
    isLoading,
    company,
    loggedIn,
    error
  };
}

const connectedCompanyPage = connect(mapStateToProps)(CompanyPage);
export { connectedCompanyPage as CompanyPage };
