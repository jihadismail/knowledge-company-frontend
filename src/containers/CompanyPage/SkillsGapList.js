import React from 'react';
import { connect } from 'react-redux';
import { Loading } from 'components';
import { companyActions } from 'actions';
import InfiniteScroll from 'react-infinite-scroller';

class SkillsGapList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 1,
      per_page: 10,
      skills: [],
      firstLoad: true,
      hasMore: props.companyActive
    };
  }

  componentDidMount() {
    this.props.dispatch(
      companyActions.getSkillsGap(
        this.props.companyid,
        this.state.page,
        this.state.per_page
      )
    );
  }

  componentWillReceiveProps(nextProps) {
    if (
      nextProps.companySkillsGap &&
      nextProps.companySkillsGap !== this.props.companySkillsGap
    ) {
      this.setState(prevState => {
        let newskills = prevState.skills.concat(nextProps.companySkillsGap);
        newskills = newskills.sort((a, b) => {
          if (a.members_count === b.members_count) {
            return a.members_percentage > b.members_percentage ? -1 : 1;
          } else {
            return a.members_count > b.members_count ? -1 : 1;
          }
        });
        return {
          skills: newskills,
          hasMore:
            this.props.companyActive && nextProps.companySkillsGap.length !== 0,
          firstLoad: false
        };
      });
    }
  }

  loadMore = page => {
    const newpage = page;
    this.props.dispatch(
      companyActions.getSkillsGap(
        this.props.companyid,
        newpage,
        this.state.per_page
      )
    );
    this.setState({ page: newpage });
  };

  renderSkills = () => {
    let items = this.state.skills.map((skill, index) => {
      return (
        <li className="skill-item" key={index}>
          <div className="skill-info">
            <span className="skill__name">{skill.name}</span>
            <span className="skill__employees-percent">
              Employees' Gap: {skill.members_percentage}%, {skill.members_count}{' '}
              employees
            </span>
          </div>
        </li>
      );
    });

    return items;
  };

  render() {
    const { isLoadingSkillsGap, companySkillsGap } = this.props;
    return (
      <article className="skills-data gap-skills white-ko-block">
        <h2 className="section-title">Skills gaps</h2>
        {this.state.firstLoad && isLoadingSkillsGap ? (
          <Loading inverted={true} />
        ) : (
          <ul>
            <InfiniteScroll
              pageStart={1}
              loadMore={this.loadMore}
              hasMore={this.state.hasMore}
              initialLoad={false}
              useWindow={false}
            >
              {this.renderSkills()}
              {isLoadingSkillsGap && (
                <Loading inverted={true} relative={true} />
              )}
            </InfiniteScroll>
          </ul>
        )}
      </article>
    );
  }
}

function mapStateToProps(state) {
  const { companies } = state;
  const { isLoadingSkillsGap, companySkillsGap } = companies;
  return {
    isLoadingSkillsGap,
    companySkillsGap
  };
}

const connectedSkillsGapList = connect(mapStateToProps)(SkillsGapList);
export { connectedSkillsGapList as SkillsGapList };
