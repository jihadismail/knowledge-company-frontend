import React from 'react';
import { connect } from 'react-redux';
import { Loading } from 'components';
import { companyActions } from 'actions';
import { Bar } from '@nivo/bar';
import { Pie } from '@nivo/pie';

class B2BPeopleInsights extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      analyticsId: this.props.analyticsId,
      peopleCategories: [
        'Skills',
        // 'Skills Gap',
        // 'Locations',
        'Experience',
        'Pervious Companies'
      ],
      selectedPeopleCategory: 'Skills',
      chatterMillPeopleSkills: [
        { skill: 'Management', number: 18 },
        { skill: 'Teamwork', number: 17 },
        { skill: 'Data Analysis', number: 16 },
        { skill: 'Business Strategy', number: 15 },
        { skill: 'Microsoft Excel', number: 15 },
        { skill: 'Microsoft Office', number: 15 },
        { skill: 'Business Development', number: 14 },
        { skill: 'Marketing Strategy', number: 13 },
        { skill: 'Sql', number: 13 },
        { skill: 'Project Management', number: 13 },
        { skill: 'Research', number: 13 },
        { skill: 'Social Media', number: 13 },
        { skill: 'Strategy', number: 12 },
        { skill: 'Javascript', number: 12 },
        { skill: 'Python', number: 12 },
        { skill: 'Start-Ups', number: 10 },
        { skill: 'Git', number: 10 },
        { skill: 'Account Management', number: 10 },
        { skill: 'Time Management', number: 10 }
      ],

      chatterMillPeoplePreviousCompanies: [
        {
          name: 'Potentialife',
          count: 3
        },
        {
          name: 'Entrepreneur First',
          count: 2
        },
        {
          name: 'Founders of the Future',
          count: 2
        },
        {
          name: 'Tinkoff Bank',
          count: 2
        },
        {
          name: 'Reevoo',
          count: 1
        },
        {
          name: 'DN Capital',
          count: 1
        },
        {
          name: 'HelloFresh',
          count: 1
        },
        {
          name: 'GS Group Holding Company',
          count: 1
        },
        {
          name: 'Seedrs',
          count: 1
        },
        {
          name: 'API Hogs',
          count: 1
        },
        {
          name: 'LoyaltyPlant',
          count: 1
        },
        {
          name: 'Argus Metals',
          count: 1
        },
        {
          name: 'Qraft',
          count: 1
        },
        {
          name: 'Travelping GmbH',
          count: 1
        },
        {
          name: 'Methods',
          count: 1
        },
        {
          name: 'Captiv8',
          count: 1
        },
        {
          name: 'Kalo',
          count: 1
        },
        {
          name: 'JustGiving',
          count: 1
        },
        {
          name: 'Conversocial',
          count: 1
        },
        {
          name: 'Ivalua',
          count: 1
        },
        {
          name: 'Marley Spoon',
          count: 1
        },
        {
          name: '11:FS',
          count: 1
        },
        {
          name: 'Standing Tall',
          count: 1
        },
        {
          name: 'Marvel App',
          count: 1
        },
        {
          name: 'btov Partners',
          count: 1
        },
        {
          name: 'PatSnap',
          count: 1
        },
        {
          name: 'Dekko Secure',
          count: 1
        },
        {
          name: 'Kayako',
          count: 1
        },
        {
          name: 'BIA-Technologies',
          count: 1
        },
        {
          name: 'PatSnap',
          count: 1
        },
        {
          name: 'KidStart',
          count: 1
        },
        {
          name: 'Booker Group',
          count: 1
        },
        {
          name: 'The University of Sheffield',
          count: 1
        },
        {
          name: 'Ometria',
          count: 1
        },
        {
          name: 'TrendKite',
          count: 1
        },
        {
          name: 'Microsoft',
          count: 1
        },
        {
          name: 'Perkbox',
          count: 1
        },
        {
          name: 'notonthehighstreet.com',
          count: 1
        },
        {
          name: 'Rasa',
          count: 1
        },
        {
          name: 'UCL',
          count: 1
        },
        {
          name: 'Segmento',
          count: 1
        },
        {
          name: 'Software Engineer',
          count: 1
        },
        {
          name: 'Invocable',
          count: 1
        },
        {
          name: 'Mintel',
          count: 1
        },
        {
          name: 'Dorae',
          count: 1
        },
        {
          name: 'Semantic Evolution',
          count: 1
        },
        {
          name: 'Slync',
          count: 1
        },
        {
          name: 'Wayfair',
          count: 1
        },
        {
          name: 'Anzenna.io',
          count: 1
        },
        {
          name: 'whiteBULLET Solutions Limited',
          count: 1
        }
      ],
      chatterMillPeopleExperience: [
        {
          id: '10 < years ',
          label: '10 < years ',
          value: 11
        },
        {
          id: '7 ≤ years < 10',

          label: '7 ≤ years < 10',
          value: 15
        },
        {
          id: '5 ≤ years < 7',

          label: '5 ≤ years < 7',
          value: 10
        },
        {
          id: 'years ≤ 5',
          label: 'years ≤ 5',
          value: 19
        }
      ]
    };
  }

  componentDidMount() {
    // this.props.dispatch(
    //   companyActions.getCompanyPeopleAnalytics(this.state.analyticsId)
    // );
  }

  //Actions
  handleCategorySelection = categoryType => {
    this.setState({
      selectedPeopleCategory: categoryType
    });
  };

  //helper
  getTheFirstTenItems = list => {
    if (list.count <= 10) return list;
    return list.slice(0, 10);
  };

  // Renders
  renderPeopleCategoryCharts = e => {
    const categoryType = this.state.selectedPeopleCategory;
    if (!categoryType) return <div />;

    // const { peopleAnalytics } = this.props.peopleAnalytics;
    switch (categoryType) {
      case 'Skills':
        // return this.renderSkillsCharts(
        //   this.getTheFirstTenItems(peopleAnalytics.current_skills)
        // );

        return this.renderSkillsCharts(
          this.getTheFirstTenItems(this.state.chatterMillPeopleSkills)
        );
      case 'Experience':
        // return this.renderExperienceCharts(this.getTheFirstTenItems(peopleAnalytics.years_of_experience));
        return this.renderExperienceCharts(
          this.getTheFirstTenItems(this.state.chatterMillPeopleExperience)
        );
      case 'Pervious Companies':
        // return this.renderPreviousCompanies(
        //   this.getTheFirstTenItems(peopleAnalytics.prev_companies)
        // );

        return this.renderPreviousCompanies(
          this.getTheFirstTenItems(
            this.state.chatterMillPeoplePreviousCompanies
          )
        );
      // case 'Skills Gap':
      //   return this.renderSkillsGapCharts();
      // case 'Locations':
      //   return this.renderLocationsCharts();
      default:
        return <div />;
    }
  };

  renderSkillsCharts = skills => {
    return (
      <div>
        <h3>Available skills</h3>
        <div>
          <Bar
            width={1000}
            height={600}
            margin={{ top: 60, right: 180, bottom: 60, left: 150 }}
            // data={this.state.chatterMillPeopleSkills}
            data={skills}
            labelSkipWidth={16}
            layout="horizontal"
            indexBy="name"
            keys={['count']}
            enableGridY={false}
            enableGridX={true}
            labelTextColor="inherit:darker(1.4)"
            innerPadding={10}
            colors={{ scheme: 'nivo' }}
            axisTop={null}
            axisRight={null}
            axisBottom={{
              tickSize: 5,
              tickPadding: 5,
              tickRotation: 0,
              legend: 'Number of employees',
              legendPosition: 'middle',
              legendOffset: 32
            }}
            axisLeft={{
              tickSize: 5,
              tickPadding: 5,
              tickRotation: 0,
              legend: 'Skill',
              legendPosition: 'middle',
              legendOffset: -140
            }}
            labelSkipWidth={12}
            labelSkipHeight={12}
            labelTextColor={{ from: 'color', modifiers: [['darker', 1.6]] }}
            // the chart's information
            legends={[
              {
                dataFrom: 'keys',
                anchor: 'bottom-right',
                direction: 'column',
                justify: false,
                translateX: 120,
                translateY: 0,
                itemsSpacing: 2,
                itemWidth: 100,
                itemHeight: 20,
                itemDirection: 'left-to-right',
                itemOpacity: 0.85,
                symbolSize: 20,
                effects: [
                  {
                    on: 'hover',
                    style: {
                      itemOpacity: 1
                    }
                  }
                ]
              }
            ]}
          />
        </div>
      </div>
    );
  };

  renderSkillsGapCharts = e => {
    return (
      <div>
        <h3>Skills Gap</h3>
        <div>
          <Bar
            width={1000}
            height={600}
            margin={{ top: 60, right: 80, bottom: 60, left: 150 }}
            data={this.state.chatterMillPeopleSkills}
            labelSkipWidth={16}
            layout="horizontal"
            indexBy="skill"
            keys={['number']}
            enableGridY={false}
            enableGridX={true}
            labelTextColor="inherit:darker(1.4)"
            theme={{
              axis: {
                ticks: {
                  line: { stroke: 'green' },
                  text: { fill: 'blue' }
                }
              },
              grid: {
                line: { stroke: 'pink', strokeWidth: 2, strokeDasharray: '4 4' }
              },
              axisBottom: {
                legend: 'Number of employees',
                legendPosition: 'middle',
                legendOffset: 100,
                orient: 'bottom',
                tickPadding: 20
              }
            }}
            colors={[
              '#61cdbb',
              '#e25c3b',
              '#61cdbb',
              '#e25c3b',
              '#61cdbb',
              '#e25c3b',
              '#61cdbb',
              '#e25c3b',
              '#61cdbb',
              '#e25c3b',
              '#61cdbb',
              '#e25c3b',
              '#61cdbb',
              '#e25c3b',
              '#61cdbb',
              '#e25c3b',
              '#61cdbb',
              '#e25c3b',
              '#61cdbb',
              '#e25c3b'
            ]}
          />
        </div>
      </div>
    );
  };

  renderLocationsCharts = e => {
    return (
      <div>
        <h3>Locations</h3>
        <div>
          <Bar
            width={1000}
            height={600}
            margin={{ top: 60, right: 80, bottom: 60, left: 150 }}
            data={this.state.chatterMillPeopleSkills}
            labelSkipWidth={16}
            layout="horizontal"
            indexBy="skill"
            keys={['number']}
            enableGridY={false}
            enableGridX={true}
            labelTextColor="inherit:darker(1.4)"
            theme={{
              axis: {
                ticks: {
                  line: { stroke: 'green' },
                  text: { fill: 'blue' }
                }
              },
              grid: {
                line: { stroke: 'pink', strokeWidth: 2, strokeDasharray: '4 4' }
              },
              axisBottom: {
                legend: 'Number of employees',
                legendPosition: 'middle',
                legendOffset: 100,
                orient: 'bottom',
                tickPadding: 20
              }
            }}
            colors={[
              '#61cdbb',
              '#e25c3b',
              '#61cdbb',
              '#e25c3b',
              '#61cdbb',
              '#e25c3b',
              '#61cdbb',
              '#e25c3b',
              '#61cdbb',
              '#e25c3b',
              '#61cdbb',
              '#e25c3b',
              '#61cdbb',
              '#e25c3b',
              '#61cdbb',
              '#e25c3b',
              '#61cdbb',
              '#e25c3b',
              '#61cdbb',
              '#e25c3b'
            ]}
          />
        </div>
      </div>
    );
  };

  renderExperienceCharts = yearsOfExperience => {
    return (
      <div>
        <h3>Experience</h3>
        <div>
          <Pie
            // data={this.state.chatterMillPeopleExperience}
            data={yearsOfExperience}
            // radialLabel = "name"
            // sliceLabel = "count"
            margin={{ top: 80, right: 150, bottom: 80, left: 150 }}
            width={800}
            height={800}
            innerRadius={0}
            cornerRadius={6}
            sortByValue={true}
            fir={true}
            colors={{ scheme: 'nivo' }}
            borderWidth={1}
            borderColor={{ from: 'color', modifiers: [['darker', 0.2]] }}
            radialLabelsSkipAngle={10}
            radialLabelsTextXOffset={6}
            radialLabelsTextColor="#333333"
            radialLabelsLinkOffset={0}
            radialLabelsLinkDiagonalLength={24}
            radialLabelsLinkHorizontalLength={32}
            radialLabelsLinkStrokeWidth={5}
            radialLabelsLinkColor={{ from: 'color' }}
            slicesLabelsSkipAngle={10}
            slicesLabelsTextColor="#333333"
            animate={true}
            legends={[
              {
                anchor: 'bottom',
                direction: 'row',
                translateY: 56,
                itemWidth: 100,
                itemHeight: 18,
                itemTextColor: '#999',
                symbolSize: 18,
                symbolShape: 'circle',
                effects: [
                  {
                    on: 'hover',
                    style: {
                      itemTextColor: '#000'
                    }
                  }
                ]
              }
            ]}
          />
        </div>
      </div>
    );
  };

  renderPreviousCompanies = previousCompanies => {
    return (
      <div>
        <h3>Previous Companies</h3>
        <div>
          <Bar
            width={1200}
            height={600}
            margin={{ top: 60, right: 80, bottom: 180, left: 150 }}
            // data={this.state.chatterMillPeoplePreviousCompanies}
            data={previousCompanies}
            labelSkipWidth={16}
            indexBy="name"
            keys={['count']}
            enableGridY={true}
            enableGridX={false}
            labelTextColor="inherit:darker(1.4)"
            innerPadding="0"
            colors={{ scheme: 'yellow_green_blue' }}
            axisTop={null}
            axisRight={null}
            axisBottom={{
              tickSize: 5,
              tickPadding: 5,
              tickRotation: -60,
              legend: 'Company',
              legendPosition: 'middle',
              legendOffset: 150
            }}
            axisLeft={{
              tickSize: 5,
              tickPadding: 5,
              tickRotation: 0,
              legend: 'Number of employees',
              legendPosition: 'middle',
              legendOffset: -60
            }}
            labelSkipWidth={12}
            labelSkipHeight={12}
            labelTextColor={{ from: 'color', modifiers: [['darker', 1.6]] }}
            // the chart's information
            legends={[
              {
                dataFrom: 'keys',
                anchor: 'bottom-right',
                direction: 'column',
                justify: false,
                translateX: 120,
                translateY: 0,
                itemsSpacing: 2,
                itemWidth: 100,
                itemHeight: 20,
                itemDirection: 'left-to-right',
                itemOpacity: 0.85,
                symbolSize: 20,
                effects: [
                  {
                    on: 'hover',
                    style: {
                      itemOpacity: 1
                    }
                  }
                ]
              }
            ]}
          />
        </div>
      </div>
    );
  };

  render() {
    const { isLoading, peopleAnalytics } = this.props;

    return (
      <div>
        <div class="people-insights-categories">
          {this.state.peopleCategories.map(category => {
            return (
              <button
                onClick={e => {
                  this.handleCategorySelection(category);
                }}
              >
                {category}
              </button>
            );
          })}
        </div>
        <div>{this.renderPeopleCategoryCharts()}</div>
        {/* {isLoading ? (
          <Loading inverted={true} />
        ) : peopleAnalytics ? (
          <div>{this.renderPeopleCategoryCharts()}</div>
        ) : (
          <div />
        )} */}
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { companies, authentication } = state;
  const { isLoading, error, peopleAnalytics } = companies;
  const { loggedIn } = authentication;
  return {
    isLoading,
    loggedIn,
    error,
    peopleAnalytics
  };
}

const connectedB2BPeopleInsights = connect(mapStateToProps)(B2BPeopleInsights);
export { connectedB2BPeopleInsights as B2BPeopleInsights };
