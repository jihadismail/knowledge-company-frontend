import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Loading, CharacterCircle } from 'components';
import { companyActions } from 'actions';
import { InviteEmployeeModal } from 'components';
import InfiniteScroll from 'react-infinite-scroller';
import Avatar from 'react-avatar';

import employee_placeholder from './images/employee_placeholder.png';

class EmployeesLists extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentTabIndex: 0,
      inviteEmployeeModalIsVisible: false,
      employees: [],
      hasMore: props.companyActive,
      page: 1,
      per_page: 10,
      personaid: null,
      firstLoad: true
    };
  }

  componentDidMount() {
    const settingsTabLink = document.querySelectorAll(
      '.employees-listing__tab-link li a'
    );
    if (settingsTabLink.length > 0) {
      settingsTabLink[this.state.currentTabIndex].classList.add('current');
    }

    this.props.dispatch(
      companyActions.getEmployees(
        this.props.companyid,
        null,
        this.state.page,
        this.state.per_page
      )
    );
    this.props.dispatch(companyActions.getPersonas(this.props.companyid));
  }

  componentWillReceiveProps(nextProps) {
    if (
      nextProps.companyEmployees &&
      nextProps.companyEmployees !== this.props.companyEmployees
    ) {
      this.setState(prevState => {
        const newemployees = prevState.employees.concat(
          nextProps.companyEmployees
        );
        return {
          employees: newemployees,
          hasMore:
            this.props.companyActive && nextProps.companyEmployees.length !== 0,
          firstLoad: false
        };
      });
    }
  }

  handleClickInviteEmployees = () => {
    this.setState({
      inviteEmployeeModalIsVisible: true
    });
  };

  handleHideInvitationModal = () => {
    this.setState({
      inviteEmployeeModalIsVisible: false
    });
  };

  handleTabChange = e => {
    e.preventDefault();
    const settingsTabLink = document.querySelectorAll(
      '.employees-listing__tab-link li a'
    );
    settingsTabLink.forEach(listItem => {
      listItem.classList.remove('current');
    });
    const personaId =
      e.target.dataset.tabName === 'null' ? null : e.target.dataset.tabName;
    const newpage = 1;
    settingsTabLink[e.target.dataset.tabIndex].classList.add('current');

    if (
      this.state.personaid === personaId &&
      this.this.props.isLoadingEmployees
    ) {
      return;
    }

    this.props.dispatch(
      companyActions.getEmployees(
        this.props.companyid,
        personaId,
        newpage,
        this.state.per_page
      )
    );
    this.setState({
      currentTabIndex: e.target.dataset.tabIndex,
      page: newpage,
      personaid: personaId,
      employees: [],
      firstLoad: true
    });
  };

  handleClickGetInTouch = () => {
    this.props.handleClickGetInTouch();
  };

  handleEmployee = e => {
    if (!this.props.companyActive) {
      e.preventDefault();
      this.props.handleMore();
    }
    if (this.props.companyActive && this.state.currentTabIndex === 0) {
      //e.preventDefault();
      //this.props.handleInvite();
    }
  };

  loadMore = page => {
    const newpage = page;
    this.props.dispatch(
      companyActions.getEmployees(
        this.props.companyid,
        this.state.personaid,
        newpage,
        this.state.per_page
      )
    );
    this.setState({ page: newpage });
  };

  renderEmployees = () => {
    const { loggedIn, companyActive } = this.props;
    let items = [];
    if (!companyActive) {
      items.push(
        <li className="skdb">
          <CharacterCircle
            avatarSize={'c-60'}
            backgroundColor={'dark'}
            speechBubbleMessage={
              'We’ve analysed the a small section of your workforce. Sign up to see all employees and help them improve their skills!'
            }
            characterClass={'circle-character-man'}
          />
        </li>
      );
    }
    if (this.state.employees.length > 0) {
      items = items.concat(
        this.state.employees.map((employee, index) => {
          let image_url = employee.profile_image_url;
          if (!image_url) {
            image_url = employee_placeholder;
          }

          let employeeUrl = '';
          if (companyActive) {
            if (employee.persona) {
              employeeUrl = `/${this.props.companyPermalink}/members/${
                employee.permalink
              }`;
            } else {
              employeeUrl = `/${this.props.companyPermalink}/unsigned_members/${
                employee.permalink
              }`;
            }
          }

          let putS = true;
          if (employee.job.title[employee.job.title.length - 1] === 's') {
            putS = false;
          }

          let nameTooltip = `${employee.job.skills_percentage}% of ${
            employee.job.title
          }'${putS ? 's' : ''} skills`;

          return (
            <li className="employee" key={index} data-index={index}>
              <figure className="employee__photo">
                <Link onClick={this.handleEmployee} to={employeeUrl}>
                  <Avatar name={employee.name} size={60} maxInitials={2} />
                </Link>
              </figure>
              <div className="employee__info">
                <h4 title={nameTooltip}>
                  <Link onClick={this.handleEmployee} to={employeeUrl}>
                    {employee.name}
                  </Link>
                </h4>
                <p>
                  <strong>{employee.job.title}</strong>
                </p>
                {employee.skills_gap && (
                  <p className="opacity-4">
                    {' '}
                    <b>Skills Gaps: </b>
                    {employee.skills_gap &&
                      employee.skills_gap.map((skill, index) => {
                        if (index === employee.skills.length - 1) {
                          return `${skill}`;
                        } else {
                          return `${skill}, `;
                        }
                      })}
                  </p>
                )}
              </div>
            </li>
          );
        })
      );
    }
    return items;
  };

  render() {
    const {
      isLoadingEmployees,
      companyEmployees,
      loggedIn,
      personas,
      companyActive
    } = this.props;

    const blurredEmployeeCount = 4;
    let showGetInTouchButton = false;
    const { inviteEmployeeModalIsVisible } = this.state;

    let inviteEmployeeModal = null;
    if (inviteEmployeeModalIsVisible) {
      inviteEmployeeModal = (
        <InviteEmployeeModal
          handleHideInvitationModal={this.handleHideInvitationModal}
        />
      );
    }

    if (
      !isLoadingEmployees &&
      companyEmployees &&
      !companyActive &&
      companyEmployees.length >= blurredEmployeeCount + 1
    ) {
      showGetInTouchButton = true;
      setTimeout(() => {
        for (let i = blurredEmployeeCount; i > 0; i--) {
          if (
            document.querySelector(
              `li[data-index="${companyEmployees.length - i}"]`
            )
          ) {
            document
              .querySelector(`li[data-index="${companyEmployees.length - i}"]`)
              .classList.add('blur-this');
          }
        }
      }, 1000);
    }

    return (
      <section className="content-wrapper employee-lists white-ko-block">
        <h2 className="section-title">Employees</h2>
        {companyActive && (
          <ul className="employees-listing__tab-link">
            <li>
              <h3>
                <a
                  data-tab-index="0"
                  data-tab-name="null"
                  onClick={this.handleTabChange}
                >
                  {' '}
                  Not Signed up{' '}
                </a>
              </h3>
            </li>
            {personas &&
              personas.map((persona, index) => {
                return (
                  <li key={index}>
                    <h3>
                      <a
                        data-tab-index={index + 1}
                        data-tab-name={persona.id}
                        onClick={this.handleTabChange}
                      >
                        {' '}
                        {persona.name}
                      </a>
                    </h3>
                  </li>
                );
              })}
          </ul>
        )}
        <article className="employees-listing__tab">
          {this.state.firstLoad && isLoadingEmployees ? (
            <Loading inverted={true} />
          ) : (
            <Fragment>
              {inviteEmployeeModal}
              {this.state.employees.length === 0 && (
                <article className="invite-employees-message center">
                  <p className="opacity-5">
                    Invite members of your team to improve your company data and
                    help them boost their skills.
                  </p>
                  <button
                    className="ko-btn-primary"
                    onClick={this.handleClickInviteEmployees}
                  >
                    Invite employees
                  </button>
                </article>
              )}
              {this.state.employees.length > 0 && (
                <ul>
                  <InfiniteScroll
                    pageStart={1}
                    loadMore={this.loadMore}
                    hasMore={this.state.hasMore}
                    initialLoad={false}
                    useWindow={false}
                  >
                    {this.renderEmployees()}
                    {isLoadingEmployees && (
                      <Loading inverted={true} relative={true} />
                    )}
                  </InfiniteScroll>
                </ul>
              )}
              {showGetInTouchButton && (
                <button
                  className="ko-btn-primary get-in-touch-btn"
                  onClick={this.handleClickGetInTouch}
                >
                  Get in touch
                </button>
              )}
            </Fragment>
          )}
        </article>
      </section>
    );
  }
}

function mapStateToProps(state) {
  const { companies, authentication } = state;
  const { loggedIn } = authentication;
  const {
    isLoadingEmployees,
    companyEmployees,
    isLoadingPersonas,
    personas
  } = companies;
  return {
    isLoadingEmployees,
    companyEmployees,
    loggedIn,
    isLoadingPersonas,
    personas
  };
}

const connectedEmployeesLists = connect(mapStateToProps)(EmployeesLists);
export { connectedEmployeesLists as EmployeesLists };
