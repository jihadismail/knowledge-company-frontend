import React from 'react';
import { connect } from 'react-redux';
import { Loading } from 'components';
import { companyActions } from 'actions';
import InfiniteScroll from 'react-infinite-scroller';

class TopSkillsList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 1,
      per_page: 10,
      skills: [],
      firstLoad: true,
      hasMore: props.companyActive
    };
  }

  componentDidMount() {
    this.props.dispatch(
      companyActions.getTopSkills(
        this.props.companyid,
        this.state.page,
        this.state.per_page
      )
    );
  }

  componentWillReceiveProps(nextProps) {
    if (
      nextProps.companyTopSkills &&
      nextProps.companyTopSkills !== this.props.companyTopSkills
    ) {
      this.setState(prevState => {
        let newskills = prevState.skills.concat(nextProps.companyTopSkills);
        newskills = newskills.sort((a, b) => {
          return a.members_count > b.members_count ? -1 : 1;
        });
        return {
          skills: newskills,
          hasMore:
            this.props.companyActive && nextProps.companyTopSkills.length !== 0,
          firstLoad: false
        };
      });
    }
  }

  calcWidth = membersCount => {
    let basisWidth = this.state.skills[0]['members_count'];
    return `${membersCount / basisWidth * 100}%`;
  };

  loadMore = page => {
    const newpage = page;
    this.props.dispatch(
      companyActions.getTopSkills(
        this.props.companyid,
        newpage,
        this.state.per_page
      )
    );
    this.setState({ page: newpage });
  };

  renderSkills = () => {
    let items = this.state.skills.map((skill, index) => {
      return (
        <li className="skill-item" key={index}>
          <div className="skill-progress">
            <span
              className="progress-bar"
              style={{ width: this.calcWidth(skill.members_count) }}
            />
          </div>
          <div className="skill-info">
            <span className="opacity-4">{skill.name}</span>
            <span>{skill.members_count} employees</span>
          </div>
        </li>
      );
    });

    return items;
  };

  render() {
    const { isLoadingTopSkills, companyTopSkills } = this.props;

    return (
      <article className="top-skills white-ko-block">
        <h2 className="section-title">Top skills</h2>
        {this.state.firstLoad && isLoadingTopSkills ? (
          <Loading inverted={true} />
        ) : (
          <ul>
            <InfiniteScroll
              pageStart={1}
              loadMore={this.loadMore}
              hasMore={this.state.hasMore}
              initialLoad={false}
              useWindow={false}
            >
              {this.renderSkills()}
              {isLoadingTopSkills && (
                <Loading inverted={true} relative={true} />
              )}
            </InfiniteScroll>
          </ul>
        )}
      </article>
    );
  }
}

function mapStateToProps(state) {
  const { companies } = state;
  const { isLoadingTopSkills, companyTopSkills } = companies;
  return {
    isLoadingTopSkills,
    companyTopSkills
  };
}

const connectedTopSkillsList = connect(mapStateToProps)(TopSkillsList);
export { connectedTopSkillsList as TopSkillsList };
