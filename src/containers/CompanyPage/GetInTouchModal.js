import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { Modal } from 'components';
import { companyActions } from 'actions';

import illu_bg from './images/illu-getintouch.png';

class GetInTouchModal extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      emailText: '',
      phoneText: ''
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.getInTouchError) {
      this.setState({ error: true });
    }
    if (nextProps.getInTouchSuccess) {
      this.setState({ success: true });
    }
    setTimeout(() => this.handleDismissModal(), 4000);
  }

  handleDismissModal = () => {
    this.props.handleHideGetInTouch();
  };

  handleEmailChange = event => {
    this.setState({ emailText: event.target.value });
  };

  handlePhoneChange = event => {
    this.setState({ phoneText: event.target.value });
  };

  handleSubmit = event => {
    event.preventDefault();
    if (this.state.emailText) {
      const organization_admin = {
        email: this.state.emailText,
        phone_number: this.state.phoneText,
        name: '',
        organization_id: this.props.companyid
      };
      this.props.dispatch(
        companyActions.getInTouch(this.props.companyid, organization_admin)
      );
    }
  };

  render() {
    return (
      <Modal
        key={'introModal'}
        showCloseButton={true}
        showSecondayDismissButton={false}
        dismissButtonText={'Not right now'}
        modalClass={'get-in-touch-modal'}
        onDismiss={this.handleDismissModal}
      >
        <img src={illu_bg} className="illu" alt="" />
        <h1>Get in touch to level up your workforce</h1>
        <p>
          Just enter your email and phone number below and we’ll be in contact
          to set up your account
        </p>
        {this.state.success || this.state.error ? (
          <Fragment>
            {this.state.success && (
              <h4 style={{ color: 'light green' }}>
                Thanks! A member of our team will be in touch.
              </h4>
            )}
            {this.state.error && (
              <h4 style={{ color: 'red' }}>Sorry, something went wrong</h4>
            )}
          </Fragment>
        ) : (
          <form onSubmit={this.handleSubmit}>
            <div className="form-block">
              <label for="email">
                <i class="fas fa-envelope" />
                <span>Enter your email</span>
              </label>
              <input
                type="email"
                className="form-control"
                name="email"
                id="email"
                placeholder="Enter your email"
                onChange={this.handleEmailChange}
              />
            </div>
            <div className="form-block">
              <label for="phone">
                <i class="fas fa-phone" />
                <span>Enter your email</span>
              </label>
              <input
                type="number"
                className="form-control"
                name="phone"
                id="phone"
                placeholder="Enter your phone number"
                onChange={this.handlePhoneChange}
              />
            </div>
            <div className="action-btn-wrapper">
              <button
                onClick={this.handleSubmit}
                className="ko-btn-primary"
                disabled={this.state.emailText ? false : true}
              >
                Submit
              </button>
            </div>
          </form>
        )}
      </Modal>
    );
  }
}

function mapStateToProps(state) {
  const { companies } = state;
  const { getInTouchLoading, getInTouchSuccess, getInTouchError } = companies;
  return {
    getInTouchLoading,
    getInTouchSuccess,
    getInTouchError
  };
}

const connectedGetInTouchModal = connect(mapStateToProps)(GetInTouchModal);
export { connectedGetInTouchModal as GetInTouchModal };
