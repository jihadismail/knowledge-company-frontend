import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import { history } from 'helpers';
import { alertActions } from 'actions';

import { GuestRoute, PrivateRoute } from 'components';
import { NotFound } from 'components';
import { Header } from 'components';
import { LoginPage } from 'containers/LoginPage';
import { CompanyPage } from 'containers/CompanyPage';
import { EmployeePage } from 'containers/EmployeePage';
import { AdminSignupPage } from 'containers/AdminSignupPage';
import { PrivacyPolicyPage } from 'containers/PrivacyPolicyPage';
import { CookiePolicyPage } from 'containers/CookiePolicyPage';

import './App.css';
import './fonts.css';
import './hover-scss/hover.css';
import './global-styles.css';
import './form-styles.css';
import './footer.css';

class App extends React.Component {
  constructor(props) {
    super(props);

    const { dispatch } = this.props;
    history.listen((location, action) => {
      // clear alert on location change
      dispatch(alertActions.clear());
    });
  }

  componentDidMount() {
    const user = JSON.parse(localStorage.getItem('user'));

    if (user) {
      // woopra_change_user(user.name, user.email);
    }

    // woopra_track_event('Any action on KO', { user: user ? true : false });
  }

  track_click = social => {
    // woopra_track_event('click social link ' + social);
  };

  render() {
    const { alert } = this.props;

    return (
      <Router history={history}>
        <div className="page-wrapper">
          {alert.message && (
            <div className={`alert ${alert.type}`}>{alert.message}</div>
          )}

          <Header />
          <main>
            <Switch>
              <GuestRoute exact path="/login" component={LoginPage} />
              <Route
                exact
                path="/privacy-policy"
                component={PrivacyPolicyPage}
              />
              <Route exact path="/cookie-policy" component={CookiePolicyPage} />
              <GuestRoute
                exact
                path="/set_password/:token"
                component={AdminSignupPage}
              />
              <PrivateRoute
                exact
                path="/:company_id/unsigned_members/:id"
                component={EmployeePage}
                isMember={false}
              />
              <PrivateRoute
                exact
                path="/:company_id/members/:id"
                component={EmployeePage}
                isMember={true}
              />
              <Route exact path="/:id" component={CompanyPage} />
              <Route path="*" component={NotFound} />
            </Switch>
          </main>
          <footer>
            <p>
              {/* <span>© 2018 Knowledge Officer</span> */}
              <Link to="/privacy-policy" target="_top">
                Privacy Policy
              </Link>
              <Link to="/cookie-policy" target="_top">
                Cookie Policy
              </Link>
            </p>
            <nav className="social-profiles">
              <a
                href="https://medium.com/knowledge-officer"
                className="hvr-radial-out"
                target="_blank"
                rel="noopener noreferrer"
                onClick={() => this.track_click('medium')}
              >
                <i className="fab fa-medium-m" />
              </a>
              <a
                href="https://twitter.com/ko_platform"
                className="hvr-radial-out"
                target="_blank"
                rel="noopener noreferrer"
                onClick={() => this.track_click('twitter')}
              >
                <i className="fab fa-twitter" />
              </a>
              <a
                href="https://www.facebook.com/knowledgeofficer/"
                className="hvr-radial-out"
                target="_blank"
                rel="noopener noreferrer"
                onClick={() => this.track_click('facebook')}
              >
                <i className="fab fa-facebook-f" />
              </a>
              <a
                href="https://www.linkedin.com/company/19861209/"
                className="hvr-radial-out"
                target="_blank"
                rel="noopener noreferrer"
                onClick={() => this.track_click('linkedin')}
              >
                <i className="fab fa-linkedin-in" />
              </a>
            </nav>
          </footer>
        </div>
      </Router>
    );
  }
}

function mapStateToProps(state) {
  const { alert } = state;
  return {
    alert
  };
}

const connectedApp = connect(mapStateToProps)(App);
export { connectedApp as App };
