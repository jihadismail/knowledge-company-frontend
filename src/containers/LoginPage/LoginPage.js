import React from 'react';
import { connect } from 'react-redux';
import { GradientScreen, CharacterBlock } from 'components';
import { adminActions } from 'actions';

import '../AdminSignupPage/AdminSignupPage.css';

class LoginPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: ''
    };
  }

  componentDidMount() {
    //this.props.match.params.token
  }

  handleChange = e => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  };

  handleSubmit = event => {
    event.preventDefault();

    if (this.state.email && this.state.password) {
      let params = {
        email: this.state.email,
        password: this.state.password
      };
      this.props.dispatch(adminActions.login(params));
    }
  };
  render() {
    return (
      <div id="adminAuth">
        <GradientScreen
          showCloseButton={false}
          showSecondayDismissButton={false}
          dismissButtonText={''}
          dismissButtonColor={'dark'}
          onDismiss={this.handleDismissGradientScreen}
          screenClass={''}
        >
          <CharacterBlock
            speechBubbleMessage={`Login to view your company's skills progress.`}
            characterClasses={'character all-characters'}
            showHill={true}
            showLogo={true}
          >
            <form>
              <div className="form-block">
                <label for="loginId">
                  <i class="fas fa-user" />
                  <span>Enter a password</span>
                </label>
                <input
                  type="text"
                  className="form-control"
                  name="email"
                  id="email"
                  placeholder="Enter your admin email"
                  required
                  onChange={this.handleChange}
                />
              </div>
              <div className="form-block">
                <label for="password">
                  <i class="fas fa-unlock-alt" />
                  <span>Enter a password</span>
                </label>
                <input
                  type="password"
                  className="form-control"
                  name="password"
                  id="password"
                  placeholder="Enter your password"
                  required
                  onChange={this.handleChange}
                />
              </div>
              <div className="action-btn-wrapper">
                <button
                  onClick={this.handleSubmit}
                  disabled={
                    this.state.email && this.state.password ? false : true
                  }
                  className="ko-btn-primary wihout-shadow"
                >
                  Login
                </button>
              </div>
            </form>
          </CharacterBlock>
        </GradientScreen>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {};
}

const connectedLoginPage = connect(mapStateToProps)(LoginPage);
export { connectedLoginPage as LoginPage };
