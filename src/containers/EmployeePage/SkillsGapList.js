import React from 'react';
import { connect } from 'react-redux';
import { Loading } from 'components';
import { employeeActions } from 'actions';
import InfiniteScroll from 'react-infinite-scroller';

class SkillsGapList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 1,
      per_page: 10,
      skills: [],
      firstLoad: true
    };
  }

  componentDidMount() {
    this.props.dispatch(
      employeeActions.getSkillsGap(
        this.props.employeeid,
        this.props.isMember,
        this.state.page,
        this.state.per_page
      )
    );
  }

  componentWillReceiveProps(nextProps) {
    if (
      nextProps.employeeSkillsGap &&
      nextProps.employeeSkillsGap !== this.props.employeeSkillsGap
    ) {
      this.setState(prevState => {
        const newskills = prevState.skills.concat(nextProps.employeeSkillsGap);
        return {
          skills: newskills,
          hasMore: nextProps.employeeSkillsGap.length !== 0,
          firstLoad: false
        };
      });
    }
  }

  loadMore = page => {
    const newpage = page;
    this.props.dispatch(
      employeeActions.getSkillsGap(
        this.props.employeeid,
        this.props.isMember,
        newpage,
        this.state.per_page
      )
    );
    this.setState({ page: newpage });
  };

  renderSkills = () => {
    let items = this.state.skills.map((skill, index) => {
      return (
        <li className="skill-item" key={index}>
          <div className="skill-info">
            <span className="skill__name">{skill.name}</span>
          </div>
        </li>
      );
    });

    return items;
  };

  render() {
    const { isLoadingSkillsGap, employeeSkillsGap } = this.props;
    return (
      <article className="skills-data gap-skills">
        <h2 className="section-title">Skills gaps</h2>
        {this.state.firstLoad && isLoadingSkillsGap ? (
          <Loading inverted={true} />
        ) : (
          <ul>
            <InfiniteScroll
              pageStart={1}
              loadMore={this.loadMore}
              hasMore={this.state.hasMore}
              initialLoad={false}
              useWindow={false}
            >
              {this.renderSkills()}
              {isLoadingSkillsGap && (
                <Loading inverted={true} relative={true} />
              )}
            </InfiniteScroll>
          </ul>
        )}
      </article>
    );
  }
}

function mapStateToProps(state) {
  const { employees } = state;
  const { isLoadingSkillsGap, employeeSkillsGap } = employees;
  return {
    isLoadingSkillsGap,
    employeeSkillsGap
  };
}

const connectedSkillsGapList = connect(mapStateToProps)(SkillsGapList);
export { connectedSkillsGapList as SkillsGapList };
