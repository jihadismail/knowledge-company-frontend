import React from 'react';
import { Fragment } from 'react';
import { connect } from 'react-redux';
import { employeeActions } from 'actions';
import { Loading } from 'components';
import { SkillsRadar } from 'components';
import { ZeroState } from 'components';
import { CharacterCircle } from 'components';
import { InviteEmployeeModal } from 'components';
import { SkillsGapList } from './SkillsGapList';
import { SkillsList } from './SkillsList';
import Avatar from 'react-avatar';

import '../CompanyPage/CompanyPage.css';

// import img_placeholder from './images/img.png';
import employee_placeholder from './images/employee_placeholder.png';

class EmployeePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      radarData: null,
      inviteEmployeeModalIsVisible: false
    };
  }

  componentDidMount() {
    this.props.dispatch(
      employeeActions.get(this.props.match.params.id, this.props.isMember)
    );
    if (this.props.isMember) {
      const skills = [
        'Design',
        'Strategy',
        'Leadership',
        'Business',
        'Development'
      ];
      this.props.dispatch(
        employeeActions.getSkillsRadar(this.props.match.params.id, skills)
      );
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.employee) {
      localStorage.setItem(
        'companyId',
        JSON.stringify(nextProps.employee.organization.permalink)
      );
    }

    if (nextProps.skillsRadarData) {
      let labels = [];
      let data = [];
      nextProps.skillsRadarData.forEach(skill => {
        const keyname = Object.keys(skill)[0];
        labels.push(keyname);
        data.push(skill[keyname]);
      });
      const radarData = {
        labels: labels,
        datasets: [
          {
            label: 'User Skills',
            backgroundColor: 'rgb(51, 153, 255, 0.3)',
            data: data
          }
        ]
      };
      this.setState({ radarData: radarData });
    }
  }

  handleClickInviteEmployees = () => {
    this.setState({
      inviteEmployeeModalIsVisible: true
    });
  };

  handleHideInvitationModal = () => {
    this.setState({
      inviteEmployeeModalIsVisible: false
    });
  };

  render() {
    const { isLoading, employee, isLoadingSkills, isMember } = this.props;

    const levelsNames = {
      '0': 'Beginner',
      '1': 'Intermediate',
      '2': 'Advanced'
    };

    let creditProgress = 0;
    if (employee && isMember) {
      creditProgress = employee.score / 100;
    }

    let inviteEmployeeModal = null;
    if (this.state.inviteEmployeeModalIsVisible) {
      inviteEmployeeModal = (
        <InviteEmployeeModal
          handleHideInvitationModal={this.handleHideInvitationModal}
        />
      );
    }

    let image_url;
    let nameTooltip = '';
    if (employee) {
      image_url = employee.profile_image_url;
      if (!image_url) {
        image_url = employee_placeholder;
      }
      let putS = true;
      if (employee.job.title[employee.job.title.length - 1] === 's') {
        putS = false;
      }
      nameTooltip = `${employee.job.skills_percentage}% of ${
        employee.job.title
      }'${putS ? 's' : ''} skills`;
    }

    return (
      <div id="companyProfile" className="employee-page">
        {isLoading ? (
          <Loading inverted={true} />
        ) : employee ? (
          <Fragment>
            {inviteEmployeeModal}
            <section className="page-insight">
              <div className="page-insight__wrapper">
                <figure className="employee__photo">
                  <a>
                    <Avatar name={employee.name} size={60} maxInitials={2} />
                  </a>
                </figure>
                <div className="employee__info" title={nameTooltip}>
                  <h1> {employee.name} </h1>
                  <span>{isMember && employee.persona.name}</span>
                </div>
              </div>
            </section>

            <div className="layout-wrapper">
              <section className="content-wrapper ko-learning-progress">
                <article className="level-progress white-ko-block">
                  <h2 className="section-title">
                    KO Learning Level:{' '}
                    <span>
                      {isMember ? employee.learning_level : 'Not signed up'}
                    </span>
                  </h2>
                  <div className="l-progress-wrapper">
                    <div className="l-progress">
                      <span
                        style={{ width: `${creditProgress}%` }}
                        className="progress-bar"
                      />
                    </div>
                    <div className="progress-text-info bottom">
                      <span class="opacity-4">
                        {employee.score ? employee.score : 0} credits
                      </span>
                    </div>
                  </div>
                </article>

                <article className="topics-progress white-ko-block">
                  <h2 className="section-title">
                    Signed up to {isMember ? employee.member_topics.length : 0}{' '}
                    topics
                  </h2>
                  <ul>
                    {isMember ? (
                      employee.member_topics.map((member_topic, index) => {
                        let percentage =
                          member_topic.learning_path.progress.percentage;
                        let complete = percentage === 100;

                        let levelcertificate = null;
                        if (complete) {
                          levelcertificate = (
                            <a onClick={e => e.preventDefault()}>
                              View certificate
                            </a>
                          );
                        } else {
                          levelcertificate = (
                            <span class="opacity-4">
                              {levelsNames[member_topic.level]} level
                            </span>
                          );
                        }
                        return (
                          <li className="topic">
                            <div className="l-progress-wrapper">
                              <div className="progress-text-info top">
                                <span class="opacity-6">
                                  {member_topic.topic.name}
                                </span>
                                {/*
                                <span class="opacity-4">
                                  avg. challenge score: 82%
                                </span>
                                */}
                              </div>
                              <div
                                className={`l-progress ${
                                  complete ? 'progress-complete' : ''
                                }`}
                              >
                                <span
                                  style={{ width: `${percentage}%` }}
                                  className="progress-bar"
                                />
                              </div>
                              <div className="progress-text-info bottom">
                                {levelcertificate}
                                <span class="opacity-4">{percentage}%</span>
                              </div>
                            </div>
                          </li>
                        );
                      })
                    ) : (
                      <ZeroState
                        text={`${
                          employee.name
                        } hasn't signed up to any topics yet. Send a Knowledge Officer invite to get started.`}
                        action={this.handleClickInviteEmployees}
                        actionText="Invite by email"
                      />
                    )}
                  </ul>
                </article>
                {!isMember && (
                  <section className="character-message">
                    <CharacterCircle
                      compactSize={true}
                      avatarSize={'c-90'}
                      backgroundColor={'light'}
                      speechBubbleMessage={`${
                        employee.name
                      } hasn't signed up to Knowledge Officer yet.`}
                      characterClass={'circle-character-woman'}
                    />
                  </section>
                )}
              </section>

              <section className="content-wrapper skills-info-graphs white-ko-block">
                <article className="skills-charts">
                  <h2 className="section-title">
                    {isMember
                      ? `${employee.job.title} skills`
                      : 'Skills Insights'}
                  </h2>
                  {isMember ? (
                    <Fragment>
                      {/*
                      <div>
                        {this.state.radarData && (
                          <SkillsRadar data={this.state.radarData} />
                        )}
                      </div>
                      */}
                      {/*<div>chart 2</div>*/}
                    </Fragment>
                  ) : (
                    <ZeroState
                      text={`${
                        employee.name
                      } hasn't signed up to any topics yet. Send a Knowledge Officer invite to get started.`}
                      action={this.handleClickInviteEmployees}
                      actionText="Invite by email"
                    />
                  )}
                </article>

                <article className="content-wrapper skills-list">
                  <SkillsList
                    isMember={isMember}
                    employeeid={this.props.match.params.id}
                  />

                  <SkillsGapList
                    isMember={isMember}
                    employeeid={this.props.match.params.id}
                  />
                </article>
              </section>
            </div>
          </Fragment>
        ) : (
          <section className="page-insight">
            <div className="page-insight__wrapper">
              <div className="employee__info">
                <h1>Employee Not Found</h1>
              </div>
            </div>
          </section>
        )}
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { employees, authentication } = state;
  const {
    isLoading,
    employee,
    isLoadingSkills,
    isLoadingSkillsRadar,
    skillsRadarData
  } = employees;
  const { loggedIn } = authentication;
  return {
    isLoading,
    employee,
    loggedIn,
    isLoadingSkills,
    isLoadingSkillsRadar,
    skillsRadarData
  };
}

const connectedEmployeePage = connect(mapStateToProps)(EmployeePage);
export { connectedEmployeePage as EmployeePage };
