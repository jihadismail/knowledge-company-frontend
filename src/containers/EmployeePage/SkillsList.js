import React from 'react';
import { connect } from 'react-redux';
import { Loading } from 'components';
import { employeeActions } from 'actions';
import InfiniteScroll from 'react-infinite-scroller';

class SkillsList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 1,
      per_page: 10,
      skills: [],
      firstLoad: true
    };
  }

  componentDidMount() {
    this.props.dispatch(
      employeeActions.getSkills(
        this.props.employeeid,
        this.props.isMember,
        this.state.page,
        this.state.per_page
      )
    );
  }

  componentWillReceiveProps(nextProps) {
    if (
      nextProps.employeeSkills &&
      nextProps.employeeSkills !== this.props.employeeSkills
    ) {
      this.setState(prevState => {
        const newskills = prevState.skills.concat(nextProps.employeeSkills);
        return {
          skills: newskills,
          hasMore: nextProps.employeeSkills.length !== 0,
          firstLoad: false
        };
      });
    }
  }

  loadMore = page => {
    const newpage = page;
    this.props.dispatch(
      employeeActions.getSkills(
        this.props.employeeid,
        this.props.isMember,
        newpage,
        this.state.per_page
      )
    );
    this.setState({ page: newpage });
  };

  renderSkills = () => {
    let items = this.state.skills.map((skill, index) => {
      return (
        <li className="skill-item">
          <div className="skill-info">
            <span className="skill__name">{skill.name}</span>
            <span className="skill__industry-coverage">
              {skill.description}
            </span>
          </div>
        </li>
      );
    });

    return items;
  };

  render() {
    const { isLoadingSkills, employeeSkills } = this.props;
    return (
      <article className="skills-data existing-skills">
        <h2 className="section-title">Skills</h2>
        {this.state.firstLoad && isLoadingSkills ? (
          <Loading inverted={true} />
        ) : (
          <ul>
            <InfiniteScroll
              pageStart={1}
              loadMore={this.loadMore}
              hasMore={this.state.hasMore}
              initialLoad={false}
              useWindow={false}
            >
              {this.renderSkills()}
              {isLoadingSkills && <Loading inverted={true} relative={true} />}
            </InfiniteScroll>
          </ul>
        )}
      </article>
    );
  }
}

function mapStateToProps(state) {
  const { employees } = state;
  const { isLoadingSkills, employeeSkills } = employees;
  return {
    isLoadingSkills,
    employeeSkills
  };
}

const connectedSkillsList = connect(mapStateToProps)(SkillsList);
export { connectedSkillsList as SkillsList };
