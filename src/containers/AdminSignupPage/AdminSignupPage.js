import React from 'react';
import { connect } from 'react-redux';
import { GradientScreen, CharacterBlock } from 'components';
import { adminActions } from 'actions';

import './AdminSignupPage.css';

class AdminSignupPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      password: ''
    };
  }

  componentDidMount() {}

  handlePasswordChange = event => {
    this.setState({ password: event.target.value });
  };

  handleSubmit = event => {
    event.preventDefault();

    if (this.state.password) {
      let params = {
        password: this.state.password,
        set_password_token: this.props.match.params.token
      };
      this.props.dispatch(adminActions.set_password(params));
    }
  };

  render() {
    const company_title = '';
    return (
      <div id="adminAuth">
        <GradientScreen
          showCloseButton={false}
          showSecondayDismissButton={false}
          dismissButtonText={''}
          dismissButtonColor={'dark'}
          screenClass={''}
        >
          <CharacterBlock
            speechBubbleMessage={
              'Hi! thanks for joining us! Just set a password below to continue to your account.'
            }
            characterClasses={'character all-characters'}
            showHill={true}
            showLogo={true}
          >
            <form>
              <div className="form-block">
                <label for="password">
                  <i class="fas fa-unlock-alt" />
                  <span>Enter a password</span>
                </label>
                <input
                  type="password"
                  className="form-control"
                  name="password"
                  id="password"
                  placeholder="Enter a password"
                  required
                  onChange={this.handlePasswordChange}
                />
              </div>
              <div className="action-btn-wrapper">
                <button
                  className="ko-btn-primary wihout-shadow"
                  disabled={this.state.password.length > 5 ? false : true}
                  onClick={this.handleSubmit}
                >
                  Continue
                </button>
              </div>
            </form>
          </CharacterBlock>
        </GradientScreen>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { admins } = state;
  const { admin, error } = admins;
  return {
    admin,
    error
  };
}

const connectedAdminSignupPage = connect(mapStateToProps)(AdminSignupPage);
export { connectedAdminSignupPage as AdminSignupPage };
