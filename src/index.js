import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { App } from './containers';
//import registerServiceWorker from './registerServiceWorker';
import { store } from './helpers';

import { CookiesProvider } from 'react-cookie';

import Raven from 'raven-js';
import { sentry_url } from './config/sentry';

Raven.config(sentry_url).install();

ReactDOM.render(
  <CookiesProvider>
    <Provider store={store}>
      <App />
    </Provider>
  </CookiesProvider>,
  document.getElementById('root')
);

window.dataLayer = window.dataLayer || [];
// function gtag() {
//   window.dataLayer.push(arguments);
// }

// google analytics
// gtag('js', new Date());
// gtag('config', 'UA-115790282-1');

//for development
if (module.hot) {
  module.hot.accept();
}
