import Raven from 'raven-js';

const sentry_key = '6d55f73fcae04aac8faaf2350b18db4b';
const sentry_app = '1310053';
export const sentry_url = `https://${sentry_key}@app.getsentry.com/${sentry_app}`;

// to be used for customized messages with more informations
export function logException(ex, context) {
  Raven.captureException(ex, {
    extra: context
  });

  window && window.console && console.error && console.error(ex);
}
