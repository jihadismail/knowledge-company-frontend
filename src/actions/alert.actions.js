import { alertConstants } from '../constants';

export const alertActions = {
  success,
  error,
  clear
};

function success(message, dispatch) {
  setTimeout(function() {
    dispatch(clear());
  }, 3000);
  return { type: alertConstants.SUCCESS, message };
}

function error(message, dispatch) {
  setTimeout(function() {
    dispatch(clear());
  }, 3000);
  return { type: alertConstants.ERROR, message };
}

function clear() {
  return { type: alertConstants.CLEAR };
}
