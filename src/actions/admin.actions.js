import { adminConstants } from '../constants';
import { adminService } from 'services';
import { history } from 'helpers';
import { alertActions } from './';

export const adminActions = {
  set_password,
  login,
  logout
};

function set_password(params) {
  return dispatch => {
    dispatch(request());

    adminService.set_password(params).then(
      admin => {
        history.push('/' + admin.organization.permalink);
        dispatch(success(admin));
      },
      error => {
        dispatch(failure(error));
        throw error;
      }
    );
  };

  function request() {
    return { type: adminConstants.SET_PASSWORD_REQUEST };
  }
  function success(admin) {
    return { type: adminConstants.SET_PASSWORD_SUCCESS, admin };
  }
  function failure(error) {
    return { type: adminConstants.SET_PASSWORD_FAILURE, error };
  }
}

function login(params) {
  return dispatch => {
    dispatch(request());

    adminService.login(params).then(
      admin => {
        history.push('/' + admin.organization.permalink);
        dispatch(success(admin));
      },
      error => {
        dispatch(failure(error));
        dispatch(alertActions.error(error, dispatch));
        throw error;
      }
    );
  };

  function request() {
    return { type: adminConstants.LOGIN_REQUEST };
  }
  function success(admin) {
    return { type: adminConstants.LOGIN_SUCCESS, admin };
  }
  function failure(error) {
    return { type: adminConstants.LOGIN_FAILURE, error };
  }
}

function logout() {
  adminService.logout();
  history.push('/login');
  return { type: adminConstants.LOGOUT };
}
