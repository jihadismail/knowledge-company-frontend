import { employeeConstants } from '../constants';
import { employeeService } from 'services';

export const employeeActions = {
  get,
  getSkills,
  getSkillsGap,
  getSkillsRadar
};

function get(employeeId, isMember) {
  return dispatch => {
    dispatch(request());

    employeeService.get(employeeId, isMember).then(
      employee => {
        dispatch(success(employee));
      },
      error => {
        dispatch(failure(error));
        throw error;
      }
    );
  };

  function request() {
    return { type: employeeConstants.GET_REQUEST };
  }
  function success(employee) {
    return { type: employeeConstants.GET_SUCCESS, employee };
  }
  function failure(error) {
    return { type: employeeConstants.GET_FAILURE, error };
  }
}

function getSkills(employeeId, isMember, page, per_page) {
  return dispatch => {
    dispatch(request());

    employeeService.getSkills(employeeId, isMember, page, per_page).then(
      employeeSkills => {
        dispatch(success(employeeSkills));
      },
      error => {
        dispatch(failure(error));
        throw error;
      }
    );
  };

  function request() {
    return { type: employeeConstants.GET_SKILLS_REQUEST };
  }
  function success(employeeSkills) {
    return { type: employeeConstants.GET_SKILLS_SUCCESS, employeeSkills };
  }
  function failure(error) {
    return { type: employeeConstants.GET_SKILLS_FAILURE, error };
  }
}

function getSkillsGap(employeeId, isMember, page, per_page) {
  return dispatch => {
    dispatch(request());

    employeeService.getSkillsGap(employeeId, isMember, page, per_page).then(
      employeeSkillsGap => {
        dispatch(success(employeeSkillsGap));
      },
      error => {
        dispatch(failure(error));
        throw error;
      }
    );
  };

  function request() {
    return { type: employeeConstants.GET_SKILLS_GAP_REQUEST };
  }
  function success(employeeSkillsGap) {
    return {
      type: employeeConstants.GET_SKILLS_GAP_SUCCESS,
      employeeSkillsGap
    };
  }
  function failure(error) {
    return { type: employeeConstants.GET_SKILLS_GAP_FAILURE, error };
  }
}

function getSkillsRadar(employeeId, skills) {
  return dispatch => {
    dispatch(request());

    employeeService.getSkillsRadar(employeeId, skills).then(
      skillsRadarData => {
        dispatch(success(skillsRadarData));
      },
      error => {
        dispatch(failure(error));
        throw error;
      }
    );
  };

  function request() {
    return { type: employeeConstants.GET_SKILLS_RADAR_REQUEST };
  }
  function success(skillsRadarData) {
    return {
      type: employeeConstants.GET_SKILLS_RADAR_SUCCESS,
      skillsRadarData
    };
  }
  function failure(error) {
    return { type: employeeConstants.GET_SKILLS_RADAR_FAILURE, error };
  }
}
