import { companyConstants } from '../constants';
import { companyService } from 'services';

export const companyActions = {
  get,
  getTopSkills,
  getSkillsGap,
  getEmployees,
  getInTouch,
  getPersonas,
  inviteMembers,
  getCompanyPeopleAnalytics
};

function get(companyId) {
  return dispatch => {
    dispatch(request());

    companyService.get(companyId).then(
      company => {
        dispatch(success(company));
      },
      error => {
        dispatch(failure(error));
        if (error !== 'Access denied') {
          throw error;
        }
      }
    );
  };

  function request() {
    return { type: companyConstants.GET_REQUEST };
  }
  function success(company) {
    return { type: companyConstants.GET_SUCCESS, company };
  }
  function failure(error) {
    return { type: companyConstants.GET_FAILURE, error };
  }
}

function getCompanyPeopleAnalytics(companyAnalyticsId) {
  return dispatch => {
    dispatch(request());

    companyService.getCompanyPeopleAnalytics(companyAnalyticsId).then(
      peopleAnalytics => {
        dispatch(success(peopleAnalytics));
      },
      error => {
        dispatch(failure(error));
        if (error !== 'Access denied') {
          throw error;
        }
      }
    );
  };

  function request() {
    return { type: companyConstants.GET_COMPANY_ANALYTICS_REQUEST };
  }

  function success(peopleAnalytics) {
    return {
      type: companyConstants.GET_COMPANY_ANALYTICS_SUCCESS,
      peopleAnalytics
    };
  }

  function failure(error) {
    return { type: companyConstants.GET_COMPANY_ANALYTICS_FAILURE, error };
  }
}

function getTopSkills(companyId, page, per_page) {
  return dispatch => {
    dispatch(request());

    companyService.getTopSkills(companyId, page, per_page).then(
      top_skills => {
        dispatch(success(top_skills));
      },
      error => {
        dispatch(failure(error));
        throw error;
      }
    );
  };

  function request() {
    return { type: companyConstants.GET_TOP_SKILLS_REQUEST };
  }
  function success(companyTopSkills) {
    return { type: companyConstants.GET_TOP_SKILLS_SUCCESS, companyTopSkills };
  }
  function failure(error) {
    return { type: companyConstants.GET_TOP_SKILLS_FAILURE, error };
  }
}

function getSkillsGap(companyId, page, per_page) {
  return dispatch => {
    dispatch(request());

    companyService.getSkillsGap(companyId, page, per_page).then(
      skills_gap => {
        dispatch(success(skills_gap));
      },
      error => {
        dispatch(failure(error));
        throw error;
      }
    );
  };

  function request() {
    return { type: companyConstants.GET_SKILLS_GAP_REQUEST };
  }
  function success(companySkillsGap) {
    return { type: companyConstants.GET_SKILLS_GAP_SUCCESS, companySkillsGap };
  }
  function failure(error) {
    return { type: companyConstants.GET_SKILLS_GAP_FAILURE, error };
  }
}

function getEmployees(companyId, personaId, page, per_page) {
  return dispatch => {
    dispatch(request());

    companyService.getEmployees(companyId, personaId, page, per_page).then(
      employees => {
        dispatch(success(employees));
      },
      error => {
        dispatch(failure(error));
        throw error;
      }
    );
  };

  function request() {
    return { type: companyConstants.GET_EMPLOYEES_REQUEST };
  }
  function success(employees) {
    return { type: companyConstants.GET_EMPLOYEES_SUCCESS, employees };
  }
  function failure(error) {
    return { type: companyConstants.GET_EMPLOYEES_FAILURE, error };
  }
}

function getInTouch(companyId, contact) {
  return dispatch => {
    dispatch(request());

    companyService.getInTouch(companyId, contact).then(
      () => {
        dispatch(success());
      },
      error => {
        dispatch(failure(error));
        throw error;
      }
    );
  };

  function request() {
    return { type: companyConstants.GET_INTOUCH_REQUEST };
  }
  function success() {
    return { type: companyConstants.GET_INTOUCH_SUCCESS };
  }
  function failure(error) {
    return { type: companyConstants.GET_INTOUCH_FAILURE, error };
  }
}

function getPersonas(companyId) {
  return dispatch => {
    dispatch(request());

    companyService.getPersonas(companyId).then(
      personas => {
        dispatch(success(personas));
      },
      error => {
        dispatch(failure(error));
        throw error;
      }
    );
  };

  function request() {
    return { type: companyConstants.GET_PERSONAS_REQUEST };
  }
  function success(personas) {
    return { type: companyConstants.GET_PERSONAS_SUCCESS, personas };
  }
  function failure(error) {
    return { type: companyConstants.GET_PERSONAS_FAILURE, error };
  }
}

function inviteMembers(members) {
  return dispatch => {
    dispatch(request());

    companyService.inviteMembers(members).then(
      () => {
        dispatch(success());
      },
      error => {
        dispatch(failure(error));
        throw error;
      }
    );
  };

  function request() {
    return { type: companyConstants.INVITE_MEMBERS_REQUEST };
  }
  function success() {
    return { type: companyConstants.INVITE_MEMBERS_SUCCESS };
  }
  function failure(error) {
    return { type: companyConstants.INVITE_MEMBERS_FAILURE, error };
  }
}
