import { userConstants } from '../constants';
import { userService } from 'services';
import { alertActions } from './';
import { history } from 'helpers';
// import { woopra_track_event } from 'helpers';
// import { woopra_change_user } from 'helpers';

export const userActions = {
  login,
  logout
};

function login(email, password) {
  return dispatch => {
    dispatch(request({ email }));

    //fake BE
    // setTimeout(function() {
    //   let user = {
    //     id: '1',
    //     username: 'yassingam',
    //     firstName: 'Yassin',
    //     lastName: 'Gamal',
    //     name: 'Yassin',
    //     token: 'fake-jwt-token',
    //     company: 'TrianglZ'
    //   };
    //
    //   dispatch(success(user));
    //   localStorage.setItem('user', JSON.stringify(user));
    //   history.push('/students');
    // }, 3000);

    userService.login(email, password).then(
      user => {
        // woopra_change_user(user.name, user.email);
        // woopra_track_event('login', { view_name: 'login', email: user.email });
        dispatch(success(user));
        history.push('/students');
      },
      error => {
        // woopra_track_event('login error', { error: error, view_name: 'login' });
        dispatch(failure(error));
        dispatch(alertActions.error(error, dispatch));
      }
    );
  };

  function request(user) {
    return { type: userConstants.LOGIN_REQUEST, user };
  }
  function success(user) {
    return { type: userConstants.LOGIN_SUCCESS, user };
  }
  function failure(error) {
    return { type: userConstants.LOGIN_FAILURE, error };
  }
}

function logout() {
  userService.logout();
  history.push('/');
  return { type: userConstants.LOGOUT };
}
