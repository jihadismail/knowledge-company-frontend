import { authHeader } from 'helpers';
import { apiUrl } from 'helpers';
import { handleResponse } from 'helpers';
import { handleConnetionError } from 'helpers';

export const employeeService = {
  get,
  getSkills,
  getSkillsGap,
  getSkillsRadar
};

function build_url(employeeId, isMember) {
  if (isMember) {
    return apiUrl() + '/members/' + employeeId;
  } else {
    return apiUrl() + '/raw_profiles/' + employeeId;
  }
}

function get(employeeId, isMember) {
  const requestOptions = {
    method: 'GET',
    headers: { ...authHeader(), 'Content-Type': 'application/json' }
  };
  return fetch(build_url(employeeId, isMember), requestOptions)
    .then(handleResponse)
    .catch(handleConnetionError);
}

function getSkills(employeeId, isMember, page, per_page) {
  const requestOptions = {
    method: 'GET',
    headers: { ...authHeader(), 'Content-Type': 'application/json' }
  };
  return fetch(
    build_url(employeeId, isMember) +
      `/skills?per_page=${per_page}&page=${page}`,
    requestOptions
  )
    .then(handleResponse)
    .catch(handleConnetionError);
}

function getSkillsGap(employeeId, isMember, page, per_page) {
  const requestOptions = {
    method: 'GET',
    headers: { ...authHeader(), 'Content-Type': 'application/json' }
  };
  return fetch(
    build_url(employeeId, isMember) +
      `/skills_gap?per_page=${per_page}&page=${page}`,
    requestOptions
  )
    .then(handleResponse)
    .catch(handleConnetionError);
}

function getSkillsRadar(employeeId, skills) {
  const requestOptions = {
    method: 'GET',
    headers: { ...authHeader(), 'Content-Type': 'application/json' }
  };
  let url = apiUrl() + '/members/' + employeeId + '/skills_radar?';
  skills.forEach(skill => {
    url = url + 'skills[]=' + skill + '&';
  });
  return fetch(url, requestOptions)
    .then(handleResponse)
    .catch(handleConnetionError);
}
