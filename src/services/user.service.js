import { authHeader } from 'helpers';
import { apiUrl } from 'helpers';
import { handleResponse } from 'helpers';
import { handleConnetionError } from 'helpers';
import { userEndPoints } from '../constants';

export const userService = {
  login,
  logout,
  getById
};

function login(email, password) {
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ email, password })
  };

  return fetch(apiUrl() + userEndPoints.USER_LOGIN, requestOptions)
    .then(handleResponse)
    .then(handleLogin)
    .catch(handleConnetionError);
}

function logout() {
  // remove user from local storage to log user out
  localStorage.removeItem('user');
}

function getById(id) {
  const requestOptions = {
    method: 'GET',
    headers: { ...authHeader(), 'Content-Type': 'application/json' }
  };

  return fetch(apiUrl() + userEndPoints.FIND_BY_ID + id, requestOptions)
    .then(handleResponse)
    .catch(handleConnetionError);
}

function handleLogin(user) {
  // login successful if there's a jwt token in the response
  if (user && user.token) {
    // store user details and jwt token in local storage to keep user logged in between page refreshes
    localStorage.setItem('user', JSON.stringify(user));
  }

  return getById(user.id).then(userData => {
    const currentUserTopic = userData.current_user_topic;
    if (currentUserTopic) {
      const userTopics = userData.user_topics;
      const currentTopicIndex = userTopics.findIndex(
        userTopic => userTopic.id === currentUserTopic
      );
      if (currentTopicIndex === -1) {
        userData.current_user_topic = 0;
      } else {
        userData.current_user_topic = currentTopicIndex;
      }
    }
    localStorage.setItem('user', JSON.stringify({ ...user, ...userData }));
    return userData;
  });
}
