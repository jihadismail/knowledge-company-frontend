import { authHeader } from 'helpers';
import { apiUrl } from 'helpers';
import { handleResponse } from 'helpers';
import { handleConnetionError } from 'helpers';

export const companyService = {
  get,
  getTopSkills,
  getSkillsGap,
  getEmployees,
  getInTouch,
  getPersonas,
  inviteMembers,
  getCompanyPeopleAnalytics
};

function get(companyId) {
  const requestOptions = {
    method: 'GET',
    headers: { ...authHeader(), 'Content-Type': 'application/json' }
  };
  return fetch(apiUrl() + '/organizations/' + companyId, requestOptions)
    .then(handleResponse)
    .catch(handleConnetionError);
}

function getCompanyPeopleAnalytics(companyAnalyticsId) {
  const requestOptions = {
    method: 'GET',
    headers: {
      'access-control-allow-origin': 'no-cors',
      host: 'data.knowledgeofficer.com',
      content_type: 'application/json'
    }
  };

  return fetch(
    `http://data.knowledgeofficer.com/analytics-api/companyInsights?company_id=${companyAnalyticsId}`,
    requestOptions
  )
    .then(handleResponse)
    .catch(handleConnetionError);
}

function getTopSkills(companyId, page, per_page) {
  const requestOptions = {
    method: 'GET',
    headers: { ...authHeader(), 'Content-Type': 'application/json' }
  };
  return fetch(
    apiUrl() +
      '/organizations/' +
      companyId +
      `/top_skills?per_page=${per_page}&page=${page}`,
    requestOptions
  )
    .then(handleResponse)
    .catch(handleConnetionError);
}

function getSkillsGap(companyId, page, per_page) {
  const requestOptions = {
    method: 'GET',
    headers: { ...authHeader(), 'Content-Type': 'application/json' }
  };
  return fetch(
    apiUrl() +
      '/organizations/' +
      companyId +
      `/skills_gap?per_page=${per_page}&page=${page}`,
    requestOptions
  )
    .then(handleResponse)
    .catch(handleConnetionError);
}

function getEmployees(companyId, personaId, page, per_page) {
  const requestOptions = {
    method: 'GET',
    headers: { ...authHeader(), 'Content-Type': 'application/json' }
  };

  let url = apiUrl() + '/organizations/' + companyId + '/unsigned_members?';
  if (personaId) {
    url =
      apiUrl() +
      '/organizations/' +
      companyId +
      '/members?persona_id=' +
      personaId;
  }
  url = url + `&per_page=${per_page}&page=${page}`;
  return fetch(url, requestOptions)
    .then(handleResponse)
    .catch(handleConnetionError);
}

function getInTouch(companyId, contact) {
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(contact)
  };
  return fetch(apiUrl() + '/organization_admins/get_in_touch', requestOptions)
    .then(handleResponse)
    .catch(handleConnetionError);
}

function getPersonas(companyId) {
  const requestOptions = {
    method: 'GET',
    headers: { ...authHeader(), 'Content-Type': 'application/json' }
  };
  return fetch(
    apiUrl() + '/organizations/' + companyId + '/personas',
    requestOptions
  )
    .then(handleResponse)
    .catch(handleConnetionError);
}

function inviteMembers(members) {
  const requestOptions = {
    method: 'POST',
    headers: { ...authHeader(), 'Content-Type': 'application/json' },
    body: JSON.stringify(members)
  };
  return fetch(apiUrl() + '/members/invite', requestOptions)
    .then(handleResponse)
    .catch(handleConnetionError);
}
