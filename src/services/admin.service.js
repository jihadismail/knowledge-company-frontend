import { apiUrl } from 'helpers';
import { handleResponse } from 'helpers';
import { handleConnetionError } from 'helpers';

export const adminService = {
  set_password,
  login,
  logout
};

function set_password(params) {
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(params)
  };
  return fetch(apiUrl() + '/organization_admins/set_password', requestOptions)
    .then(handleResponse)
    .catch(handleConnetionError);
}

function login(params) {
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(params)
  };
  return fetch(apiUrl() + '/organization_admins/login', requestOptions)
    .then(handleResponse)
    .catch(handleConnetionError);
}

function logout() {
  localStorage.removeItem('admin');
}
