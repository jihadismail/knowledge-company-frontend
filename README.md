# Knowledge Officer
Frontend Web Applicaton for **Knowledge Officer** using React.
This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).


## Table of Contents

- [Project Setup](#project-setup)
- [Folder Structure](#folder-structure)
- [Creating a New Component](#creating-a-new-component)
- [Available Scripts](#available-scripts)
  - [npm start](#npm-start)
  - [npm test](#npm-test)
  - [npm run build](#npm-run-build)

## Project Setup

* First Install *NodeJS* using [NVM](https://github.com/creationix/nvm).

* Install latest *NodeJS* *LTS* version:
```bash
nvm install --lts=Carbon
```

* Install [Yarn](https://yarnpkg.com/lang/en/docs/install/)

* Install needed packages
```bash
yarn install
```

## Folder Structure

The project structure follow the following skeleton:

```
knowledge-company/
  knowledge-company-frontend/
    README.md
    node_modules/
    package.json
    public/
      index.html
      favicon.ico
    src/
      actions/
      components/
      constants/
      containers/
        App/
          App.scss
          App.js
          index.js
      helpers/
      reducers/
      services/
      index.js
      registerServiceWorker.js
```

***Containers*** is where the main containers(screens) are defined, each in its respective directory.
Each container component is defined in *JS* file with same name as directory, a *css* file, and any needed images.
The container directory might also have other child components that are used exclusively for this container.

***components*** contains the global shared components all across the app.

***actions*** contains all the actions that are triggered from the containers. Each action file is responsible for dispatching the events, calling API functions, and dispatching the results.

***reducers*** contains all *Redux* redu`cers that will handle the state change upon dispatched events.

***services*** contains all api related calls, categorized by models.

***helpers*** contains generic *JS* functions that are used widely in the app.

***constants*** contains all the dispatched events names, and any other constants in the app.

For the project to build, **these files must exist with exact filenames**:

* `public/index.html` is the page template;
* `src/index.js` is the JavaScript entry point.

You may create subdirectories inside `src`. For faster rebuilds, only files inside `src` are processed by Webpack.<br>
You need to **put any JS and CSS files inside `src`**, otherwise Webpack won’t see them.
Each directory inside `src` must have a `index.js` file that exports the content of the directory.

Only files inside `public` can be used from `public/index.html`.<br>

## Creating a New Container Component
- To create a new *Container*, create a directory with the new container name inside `containers`.
- Create `new_container.js`, `new_container.scss`, `index.js`.

You can create more child components inside the container directory as needed, as long as they belong to the container screen and are not used in another container. If a child component will be used insie different containers it must be moved to ***components*** directory.

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br>
Open [http://localhost:3001](http://localhost:3001) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.<br>

### `npm run build-css`

Compiles the *SASS* files into css files. (Done automatically with the start command)

### `precommit`

Applies *prettier* command to staged files ready for commit. (Automatically applied on git commit command)

### `npm run build:staging`

Builds the app for **staging** to the `build` folder.<br>

### `npm run build`

Builds the app for **production** to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!
