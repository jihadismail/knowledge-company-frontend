# server-based syntax
# ======================
# Defines a single server with a list of roles and multiple properties.
# You can define all roles on a single server, or split them:
set :stage, :staging
#server "172.104.224.218", user: "kofficer-app", roles: %w{app db web}, my_property: :my_value
server "69.164.202.153", user: "api", roles: %w{app db web}, my_property: :my_value
#set :puma_bind, "tcp://0.0.0.0:9292"    #accept array for multi-bind
set :bundle_binstubs, nil
# server "example.com", user: "deploy", roles: %w{app db web}, my_property: :my_value
# server "example.com", user: "deploy", roles: %w{app web}, other_property: :other_value
# server "db.example.com", user: "deploy", roles: %w{db}
set :branch, 'staging'

namespace :deploy do
  desc 'Build Staging'
  task :build_staging do
    on roles(:app) do
      within "#{release_path}/knowledge-company-frontend" do
        execute :npm, 'run build:staging'
      end
    end
  end

  after :updated, :build_staging
end
